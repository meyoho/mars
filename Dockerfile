FROM golang:1.13 as builder

WORKDIR $GOPATH/src/mars
COPY . .
RUN GO111MODULE=off CGO_ENABLED=0 go build -a -installsuffix cgo -o $GOPATH/src/mars/bin/mars .

FROM alpine:3.11

WORKDIR /mars
RUN apk add --update ca-certificates py-pip supervisor curl

COPY --from=builder /go/src/mars/bin/mars ./mars
COPY --from=builder /go/src/mars/run/supervisord/supervisord.conf /etc/supervisord.conf

RUN mkdir -p /var/log/mathilde && \
    mkdir -p /var/log/supervisor && \
    chmod +x /mars/mars

EXPOSE 8080
CMD ["/usr/bin/supervisord", "--nodaemon", "--configuration", "/etc/supervisord.conf"]

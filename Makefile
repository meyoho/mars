OS = Linux
VERSION = 0.0.1

fmt:
	go fmt ./...

build: fmt
	docker-compose -p mars -f run/docker/mars.yaml build

up:
	- docker rm -fv mars
	docker-compose -p mars -f run/docker/mars.yaml up

compose:
	docker-compose -p mars -f run/docker/mars.yaml build
	- docker rm -fv mars
	docker-compose -p mars -f run/docker/mars.yaml up

help:
	@$(ECHO) "Targets:"
	@$(ECHO) "compose           - docker compose to build and start this project"

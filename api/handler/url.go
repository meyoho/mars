package handler

import (
	"github.com/gin-gonic/gin"
)

func AddRoutes(r *gin.Engine) {
	r.GET("/", Ping)
	r.GET("/_ping", Ping)
}

package alert

import (
	"encoding/json"

	"mars/common"
	"mars/model"

	"k8s.io/client-go/tools/cache"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

type EventHandler struct {
	Storage Storage
	Logger  *common.Logger
}

func (p *EventHandler) deleteEvent(obj interface{}) {
	var unstructuredObj interface{}
	object, ok := obj.(cache.DeletedFinalStateUnknown)
	if !ok {
		unstructuredObj = obj
	} else {
		unstructuredObj = object.Obj
	}
	event, ok := unstructuredObj.(*unstructured.Unstructured)
	if !ok {
		p.Logger.Errorf("unexpected event object from delete event")
		return
	}

	bytes, err := event.MarshalJSON()
	if err != nil {
		p.Logger.Errorf("marshal event error, %s", err.Error())
		return
	}

	var resource = &model.AlertResource{}
	if err := json.Unmarshal(bytes, resource); err != nil {
		p.Logger.Errorf("unmarshal resource error, %s", err.Error())
		return
	}

	p.Logger.Infof("Delete object: %s %s/%s", resource.Kind, resource.Namespace, resource.Name)
	p.Storage.Remove(resource)
	return
}

func (p *EventHandler) addEvent(obj interface{}) {
	var unstructuredObj interface{}
	object, ok := obj.(cache.DeletedFinalStateUnknown)
	if !ok {
		unstructuredObj = obj
	} else {
		unstructuredObj = object.Obj
	}
	event, ok := unstructuredObj.(*unstructured.Unstructured)
	if !ok {
		p.Logger.Errorf("unexpected event object from delete event")
		return
	}

	bytes, err := event.MarshalJSON()
	if err != nil {
		p.Logger.Errorf("marshal event error, %s", err.Error())
		return
	}

	var resource = &model.AlertResource{}
	if err := json.Unmarshal(bytes, resource); err != nil {
		p.Logger.Errorf("unmarshal resource error, %s", err.Error())
		return
	}

	p.Logger.Infof("Add object: %s %s/%s", resource.Kind, resource.Namespace, resource.Name)
	p.Storage.Insert(resource)
	return
}

func (p *EventHandler) updateEvent(old, new interface{}) {
	var unstructuredObj interface{}
	object, ok := new.(cache.DeletedFinalStateUnknown)
	if !ok {
		unstructuredObj = new
	} else {
		unstructuredObj = object.Obj
	}
	event, ok := unstructuredObj.(*unstructured.Unstructured)
	if !ok {
		p.Logger.Errorf("unexpected event object from delete event")
		return
	}

	bytes, err := event.MarshalJSON()
	if err != nil {
		p.Logger.Errorf("marshal event error, %s", err.Error())
		return
	}

	var resource = &model.AlertResource{}
	if err := json.Unmarshal(bytes, resource); err != nil {
		p.Logger.Errorf("unmarshal resource error, %s", err.Error())
		return
	}

	p.Logger.Infof("Add object: %s %s/%s", resource.Kind, resource.Namespace, resource.Name)
	p.Storage.Insert(resource)
	return
}

func (p *EventHandler) replaceEvent(objs []interface{}) {
	var resources []*model.AlertResource
	// Replace cached resources
	for _, obj := range objs {
		var unstructuredObj interface{}
		object, ok := obj.(cache.DeletedFinalStateUnknown)
		if !ok {
			unstructuredObj = obj
		} else {
			unstructuredObj = object.Obj
		}

		event, ok := unstructuredObj.(*unstructured.Unstructured)
		if !ok {
			p.Logger.Errorf("unexpected event object from delete event")
			return
		}

		bytes, err := event.MarshalJSON()
		if err != nil {
			p.Logger.Errorf("marshal event error, %s", err.Error())
			return
		}

		var resource = &model.AlertResource{}
		if err := json.Unmarshal(bytes, resource); err != nil {
			p.Logger.Errorf("unmarshal resource error, %s", err.Error())
			return
		}
		resources = append(resources, resource)

		p.Logger.Infof("Replace object: %s %s/%s", resource.Kind, resource.Namespace, resource.Name)
		p.Storage.Insert(resource)
	}

	// Delete resources not in replace objects
	for _, l := range p.Storage.List() {
		find := false
		for _, r := range resources {
			if r.Name == l.Name && r.Namespace == l.Namespace {
				find = true
				break
			}
		}
		if !find {
			p.Logger.Infof("Delete object: %s %s/%s", l.Kind, l.Namespace, l.Name)
			p.Storage.Remove(l)
		}
	}
	return
}

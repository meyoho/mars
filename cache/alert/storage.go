package alert

import (
	"strings"
	"sync"

	"mars/model"
)

var ruleSet = make(map[string]Storage, 0)

type Storage interface {
	List() []*model.AlertResource
	Insert(resource *model.AlertResource)
	Remove(resource *model.AlertResource)
}

type MemoryCache struct {
	cluster   *model.Cluster
	mutex     sync.RWMutex
	resources map[string]*model.AlertResource
}

func (m *MemoryCache) generateKey(resource *model.AlertResource) string {
	return strings.Join([]string{m.cluster.Name, resource.Kind, resource.Namespace, resource.Name}, "//")
}

func (m *MemoryCache) Insert(resource *model.AlertResource) {
	key := m.generateKey(resource)
	m.mutex.Lock()
	m.resources[key] = resource
	m.mutex.Unlock()
}

func (m *MemoryCache) Remove(resource *model.AlertResource) {
	key := m.generateKey(resource)
	m.mutex.Lock()
	delete(m.resources, key)
	m.mutex.Unlock()
}

func (m *MemoryCache) List() []*model.AlertResource {
	m.mutex.Lock()
	result := make([]*model.AlertResource, len(m.resources))
	i := 0
	for _, v := range m.resources {
		result[i] = v
		i++
	}
	m.mutex.Unlock()
	return result
}

func NewStorageInstance(cluster *model.Cluster) Storage {
	if _, ok := ruleSet[cluster.UUID]; ok {
		return ruleSet[cluster.UUID]
	} else {
		instance := &MemoryCache{
			cluster:   cluster.Clone(),
			resources: make(map[string]*model.AlertResource, 0),
		}
		ruleSet[cluster.UUID] = instance
		return instance
	}
}

func GetAlertResourceForCluster(c *model.Cluster) []*model.AlertResource {
	if _, ok := ruleSet[c.UUID]; !ok {
		return make([]*model.AlertResource, 0)
	}
	return ruleSet[c.UUID].List()
}

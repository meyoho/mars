package alert

import (
	"mars/infra/kubernetes"
	"mars/model"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/tools/cache"
)

func NewPrometheusRuleWatcherForCluster(c *model.Cluster) *kubernetes.ClusterWatcher {
	cw := kubernetes.NewClusterWatcher(c)
	eh := EventHandler{
		Storage: NewStorageInstance(c),
		Logger:  cw.Logger,
	}

	gvk := schema.GroupVersionKind{
		Group:   model.PrometheusRuleGroup,
		Version: model.PrometheusRuleVersion,
		Kind:    model.PrometheusRuleKind,
	}

	rc := &kubernetes.ResourceConfig{
		GroupVersionKind: gvk,
		RuntimeObject:    &unstructured.Unstructured{},
		EventsHandlers: kubernetes.ResourceEventHandlerFuncs{
			ResourceEventHandlerFuncs: cache.ResourceEventHandlerFuncs{
				AddFunc:    eh.addEvent,
				UpdateFunc: eh.updateEvent,
				DeleteFunc: eh.addEvent,
			},
			ReplaceFunc: eh.replaceEvent,
		},
	}

	cw.Logger.Infof("Try to start resource watcher")
	cw.StartResourceWatcher(rc)
	return cw
}

package common

const (
	// HTTP methods
	HttpGet    = "GET"
	HttpPost   = "POST"
	HttpPut    = "PUT"
	HttpDelete = "DELETE"
	HttpPatch  = "PATCH"

	// Kubernetes api versions
	KubernetesAPIVersionV1 = "v1"
)

package common

import (
	"strings"
)

const (
	// Source
	Source = 1090
)

const (
	// Code
	CodeInvalidArgs  = "invalid_args"
	CodeUnknownIssue = "unknown_issue"
)

type APIError struct {
	Source  int                 `json:"source"`
	Code    string              `json:"code"`
	Message string              `json:"message"`
	Detail  []map[string]string `json:"fields,omitempty"`
}

type APIErrors struct {
	Errors []*APIError `json:"errors"`
}

func (e APIErrors) Error() string {
	var messages []string
	for _, err := range e.Errors {
		messages = append(messages, err.Message)
	}
	return strings.Join(messages, "\n")
}

func BuildError(code string, message string) error {
	return APIErrors{
		Errors: []*APIError{
			{
				Source:  Source,
				Code:    code,
				Message: message,
			},
		},
	}
}

func BuildServerUnknownError(message string) error {
	return BuildError(CodeUnknownIssue, message)
}

func BuildInvalidArgsError(message string) error {
	return BuildError(CodeInvalidArgs, message)
}

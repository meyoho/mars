package common

import (
	"crypto/md5"
	"fmt"
	"strings"
)

func GetKubernetesTypeFromKind(kind string) string {
	switch strings.ToLower(kind) {
	case "networkpolicy":
		return "networkpolicies"
	case "ingress":
		return "ingresses"
	case "storageclass":
		return "storageclasses"
	case "prometheus":
		return "prometheuses"
	default:
		return strings.ToLower(kind) + "s"
	}
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func MD5(base string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(base)))
}

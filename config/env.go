package config

import (
	"io/ioutil"
	"strings"

	"github.com/vrischmann/envconfig"
)

var (
	GlobalConfig Config
)

type Config struct {
	Davion struct {
		Endpoint   string `envconfig:"default=http://davion:80"`
		ApiVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=10"`
		Enabled    bool   `envconfig:"default=true"`
	}

	ElasticSearch struct {
		Endpoint string `envconfig:"default=http://cpaas-elasticsearch:9200"`
		Username string `envconfig:"default=alaudaes"`
		Password string `envconfig:"default=es_password_1qaz2wsx"`
		Timeout  int    `envconfig:"default=20"`
	}

	Erebus struct {
		Endpoint string `envconfig:"default=https://erebus"`
	}

	Archon struct {
		Endpoint string `envconfig:"default=http://archon"`
	}

	Furion struct {
		Endpoint   string `envconfig:"default=http://furion:8080"`
		ApiVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=10"`
		SyncPeriod int    `envconfig:"default=300"`
	}

	Kubernetes struct {
		Endpoint     string `envconfig:"default=https://kubernetes.default.svc.cluster.local"`
		Token        string `envconfig:"default=token"`
		Timeout      int    `envconfig:"default=10"`
		WatchTimeout int    `envconfig:"default=600"`
		ResyncPeriod int    `envconfig:"default=90"`
	}

	Log struct {
		// Size unit: M
		Size     int    `envconfig:"default=100"`
		Level    string `envconfig:"default=info"`
		Backup   int    `envconfig:"default=2"`
		ToStdout bool   `envconfig:"default=false"`
	}

	Mars struct {
		CRDEnabled     bool   `envconfig:"default=false"`
		PodName        string `envconfig:"POD_NAME,default=morgans-5dcbff4f6f-mlhgc"`
		Namespace      string `envconfig:"NAMESPACE,default=alauda-system"`
		Debug          bool   `envconfig:"default=true"`
		MetricInterval int64  `envconfig:"default=30"`
		MetricLatency  int64  `envconfig:"default=30"`
		ExposePort     int    `envconfig:"default=8080"`
	}

	Meter struct {
		CollectorIntervalInHours   int    `envconfig:"default=2"`
		CollectorCronJobSpec       string `envconfig:"default=default"`
		AnalyzerDailyCronJobSpec   string `envconfig:"default=default"`
		AnalyzerMonthlyCronJobSpec string `envconfig:"default=default"`
	}

	Prometheus struct {
		Timeout int `envconfig:"default=30"`
	}

	Label struct {
		BaseDomain string `envconfig:"default=alauda.io"`
	}
}

func init() {
	var tokenFile = "/var/run/secrets/kubernetes.io/serviceaccount/token"
	var ESUsernameFile = "/etc/pass_es/username"
	var ESPasswordFile = "/etc/pass_es/password"

	if err := envconfig.Init(&GlobalConfig); err != nil {
		panic("Load config from env error," + err.Error())
	}

	if token, err := ioutil.ReadFile(tokenFile); err == nil {
		GlobalConfig.Kubernetes.Token = string(token)
	}

	if username, err := ioutil.ReadFile(ESUsernameFile); err == nil {
		GlobalConfig.ElasticSearch.Username = string(strings.TrimRight(string(username), "\n"))
	}

	if password, err := ioutil.ReadFile(ESPasswordFile); err == nil {
		GlobalConfig.ElasticSearch.Password = string(strings.TrimRight(string(password), "\n"))
	}
}

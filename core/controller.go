package core

import (
	"time"

	"mars/cache/alert"
	"mars/cache/cluster"
	"mars/common"
	"mars/infra/kubernetes"
	"mars/metric"
)

var watchers = make(map[string]*kubernetes.ClusterWatcher, 0)
var generators = make(map[string]*metric.Generator, 0)

func Run() {
	// Run cluster fetcher and wait for cluster fetcher ready
	logger := common.NewLogger(map[string]string{"r": "cc"})
	cluster.RunClusterCache(logger)
	time.Sleep(time.Second * 15)
	// Start watcher for clusters
	go startWatchers()
	// Start generators for clusters
	startGenerators()

}

func startWatchers() {
	logger := common.NewLogger(map[string]string{"r": "ct"})
	for {
		clusters := cluster.ListClusters()

		// Start watcher for clusters
		for _, c := range clusters {
			if _, ok := watchers[c.UUID]; !ok {
				if !c.HasMetricFeature {
					continue
				}
				logger.Infof("Start cluster watcher for cluster %s/%s", c.UUID, c.Name)
				watchers[c.UUID] = alert.NewPrometheusRuleWatcherForCluster(c)
			}
		}

		// Stop watchers unused
		for u, w := range watchers {
			find := false
			for _, c := range clusters {
				if c.UUID == u {
					find = true
					break
				}
			}
			if !find {
				logger.Infof("Stop cluster watcher for cluster %s/%s", u, u)
				w.Stop()
				delete(watchers, u)
			}
		}
		time.Sleep(time.Second * time.Duration(10))
	}
}

func startGenerators() {
	logger := common.NewLogger(map[string]string{"r": "cg"})
	for {
		clusters := cluster.ListClusters()

		// Start generator for clusters
		for _, c := range clusters {
			if _, ok := generators[c.UUID]; !ok {
				logger.Infof("Start metric generator for cluster %s/%s", c.UUID, c.Name)
				generators[c.UUID] = metric.NewGenerator(c)
			}
		}

		// Stop unused generators
		for u, g := range generators {
			find := false
			for _, c := range clusters {
				if c.UUID == u {
					find = true
					break
				}
			}
			if !find {
				logger.Infof("Stop cluster metric generator for cluster %s", u)
				g.Stop()
				delete(generators, u)
			}
		}
		time.Sleep(time.Second * time.Duration(10))
	}
}

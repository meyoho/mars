module mars

go 1.12

require (
	alauda.io/warpgate v0.0.1
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/DeanThompson/ginpprof v0.0.0-20190408063150-3be636683586
	github.com/Jeffail/gabs v1.2.0
	github.com/alauda/bergamot v0.0.0-20190314060426-2164a0440df0
	github.com/appleboy/gofight v1.0.4 // indirect
	github.com/buger/jsonparser v0.0.0-20181115193947-bf1c66bbce23 // indirect
	github.com/deckarep/golang-set v1.7.1
	github.com/elastic/go-elasticsearch/v6 v6.7.0
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/juju/errors v0.0.0-20190207033735-e65537c515d7
	github.com/juju/loggo v0.0.0-20190526231331-6e530bcce5d8 // indirect
	github.com/juju/testing v0.0.0-20190723135506-ce30eb24acd2 // indirect
	github.com/labstack/echo v3.3.10+incompatible // indirect
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/levigross/grequests v0.0.0-20190130132859-37c80f76a0da
	github.com/pborman/uuid v1.2.0
	github.com/prometheus/client_golang v0.9.2
	github.com/prometheus/common v0.0.0-20181126121408-4724e9255275
	github.com/robfig/cron v1.2.0
	github.com/sirupsen/logrus v1.4.1
	github.com/ugorji/go v1.1.4 // indirect
	github.com/vrischmann/envconfig v1.1.0
	github.com/zsais/go-gin-prometheus v0.0.0-20181030200533-58963fb32f54
	golang.org/x/oauth2 v0.0.0-20190402181905-9f3314589c9a // indirect
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4 // indirect
	gopkg.in/appleboy/gin-status-api.v1 v1.0.1
	gopkg.in/appleboy/gofight.v1 v1.0.4 // indirect
	gopkg.in/fukata/golang-stats-api-handler.v1 v1.0.0 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	k8s.io/api v0.0.0-20190416052506-9eb4726e83e4
	k8s.io/apimachinery v0.0.0-20191030190112-bb31b70367b7
	k8s.io/client-go v0.0.0-20190416092645-b7bf0a35f111
	k8s.io/utils v0.0.0-20190308190857-21c4ce38f2a7 // indirect
)

replace alauda.io/warpgate v0.0.1 => bitbucket.org/mathildetech/warpgate v0.0.1

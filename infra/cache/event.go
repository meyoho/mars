package cache

import (
	"sync"
	"time"

	"mars/common"

	"k8s.io/api/core/v1"
)

var (
	EC *EventsCache
)

func init() {
	EC = &EventsCache{
		lock:   new(sync.RWMutex),
		logger: common.NewLogger(map[string]string{"role": "events_cache"}),
		maps:   make(map[string]*NamespacedEventsCache, 0),
	}
	go EC.gc()
}

type EventsCache struct {
	lock   *sync.RWMutex
	logger *common.Logger
	maps   map[string]*NamespacedEventsCache
}

func (ec *EventsCache) Keys() []string {
	ec.lock.RLock()
	defer ec.lock.RUnlock()
	keys := make([]string, 0, len(ec.maps))
	for k := range ec.maps {
		keys = append(keys, k)
	}
	return keys
}

func (ec *EventsCache) Get(key string) *NamespacedEventsCache {
	ec.lock.RLock()
	defer ec.lock.RUnlock()
	return ec.maps[key]
}

func (ec *EventsCache) Set(key string, nc *NamespacedEventsCache) {
	ec.lock.Lock()
	defer ec.lock.Unlock()
	ec.maps[key] = nc
}

func (ec *EventsCache) Delete(key string) {
	ec.lock.Lock()
	defer ec.lock.Unlock()
	delete(ec.maps, key)
}

func (ec *EventsCache) gc() {
	for {
		time.Sleep(time.Second * 600)
		keys := ec.Keys()
		ec.logger.Infof("Find %d namespaced events cache", len(keys))
		for _, key := range keys {
			threshold := time.Now().UTC().Add(-time.Hour * 24)
			nc := ec.Get(key)
			if nc == nil {
				ec.logger.Infof("Key %s not exists, continue", key)
				continue
			}
			// If a namespaced events cache has no events and lost heart beat, remove it from event cache
			uids := nc.Keys()
			if len(uids) == 0 && nc.lastHeartbeatTime.Before(threshold) {
				ec.logger.Infof("Remove namespaced events cache %s", key)
				ec.Delete(key)
				continue
			}
			ec.logger.Infof("Find %d events in namespaced events cache %s", len(uids), key)
			// If an event has disappeared over one day, remove it from namespaced events cache
			for _, uid := range uids {
				event := nc.Get(uid)
				if event == nil {
					ec.logger.Infof("Uid %s not exists, continue", key)
					continue
				}
				last := event.LastTimestamp.Time
				if last.Before(threshold) {
					ec.logger.Infof("Remove event %s/%s from cache", event.Namespace, event.Name)
					nc.Delete(string(event.UID))
				}
			}
			// Update last transition time if needed
			if nc.lastTransitionTime.UTC().Before(threshold) {
				nc.lastTransitionTime = threshold
			}
		}
	}
}

type NamespacedEventsCache struct {
	lastTransitionTime time.Time
	lastHeartbeatTime  time.Time
	lock               *sync.RWMutex
	events             map[string]*v1.Event
}

func NewNamespacedEventsCache() *NamespacedEventsCache {
	return &NamespacedEventsCache{
		lastTransitionTime: time.Now().UTC(),
		lastHeartbeatTime:  time.Now().UTC(),
		lock:               new(sync.RWMutex),
		events:             make(map[string]*v1.Event, 0),
	}
}

func (nc *NamespacedEventsCache) LastTransitionTime() time.Time {
	nc.lock.RLock()
	defer nc.lock.RUnlock()
	return nc.lastTransitionTime
}

func (nc *NamespacedEventsCache) Keys() []string {
	nc.lock.RLock()
	defer nc.lock.RUnlock()
	keys := make([]string, 0, len(nc.events))
	for k := range nc.events {
		keys = append(keys, k)
	}
	return keys
}

func (nc *NamespacedEventsCache) Set(event *v1.Event) {
	nc.lock.Lock()
	defer nc.lock.Unlock()
	nc.events[string(event.UID)] = event
	nc.lastHeartbeatTime = time.Now().UTC()
}

func (nc *NamespacedEventsCache) Get(uid string) *v1.Event {
	nc.lock.RLock()
	defer nc.lock.RUnlock()
	nc.lastHeartbeatTime = time.Now().UTC()
	return nc.events[uid]
}

func (nc *NamespacedEventsCache) Delete(uid string) {
	nc.lock.Lock()
	defer nc.lock.Unlock()
	delete(nc.events, uid)
}

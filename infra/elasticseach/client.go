package elasticseach

import (
	"fmt"
	"net"
	"net/http"
	"time"

	"mars/common"
	"mars/config"

	"github.com/elastic/go-elasticsearch/v6"
	"k8s.io/api/core/v1"
)

type Client struct {
	Endpoint string
	Username string
	Password string
	Timeout  int
	EC       *elasticsearch.Client
	Logger   *common.Logger
}

type APISearchResponse struct {
	Took     int64          `json:"took"`
	TimedOut bool           `json:"timed_out"`
	Hits     *APISearchHits `json:"hits"`
}

type APISearchHits struct {
	Total    int64                 `json:"total"`
	MaxScore float64               `json:"max_score"`
	Hits     []*APISearchHitsEvent `json:"hits"`
}

type APISearchHitsEvent struct {
	Index string  `json:"_index"`
	Type  string  `json:"_type"`
	Id    string  `json:"_id"`
	Score float64 `json:"_score"`
	Source struct {
		Detail struct {
			ClusterName string    `json:"cluster_name"`
			Event       *v1.Event `json:"event"`
		} `json:"detail"`
	} `json:"_source"`
}

func NewClient(logger *common.Logger) *Client {
	c := &Client{
		Endpoint: config.GlobalConfig.ElasticSearch.Endpoint,
		Username: config.GlobalConfig.ElasticSearch.Username,
		Password: config.GlobalConfig.ElasticSearch.Password,
		Timeout:  config.GlobalConfig.ElasticSearch.Timeout,
		Logger:   logger,
	}

	cfg := elasticsearch.Config{
		Addresses: []string{c.Endpoint},
		Username:  c.Username,
		Password:  c.Password,
		Transport: &http.Transport{
			ResponseHeaderTimeout: time.Second * time.Duration(c.Timeout) * 2 / 3,
			DialContext:           (&net.Dialer{Timeout: time.Second * time.Duration(c.Timeout) * 1 / 3}).DialContext,
		},
	}

	es6, err := elasticsearch.NewClient(cfg)
	if err != nil {
		panic(fmt.Sprintf("New elasticsearch client error, %s", err.Error()))
	}
	c.EC = es6
	return c
}

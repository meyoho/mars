package elasticseach

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/elastic/go-elasticsearch/v6/esapi"
)

func (c *Client) InsertDocuments(indexName string, documents []Document) (int, int) {
	var (
		buf bytes.Buffer
		res *esapi.Response
		raw map[string]interface{}
		blk *BulkResponse

		numItems   int
		numErrors  int
		numIndexed int
		numBatches int
		currBatch  int
	)

	count := len(documents)
	if count%BatchSize == 0 {
		numBatches = count / BatchSize
	} else {
		numBatches = (count / BatchSize) + 1
	}
	start := time.Now().UTC()

	// Loop over the collection
	for i, d := range documents {
		numItems++

		currBatch = i / BatchSize
		if i == count-1 {
			currBatch++
		}

		// Prepare the metadata payload and data payload, encode document to JSON
		meta := []byte(fmt.Sprintf(`{ "index" : { "_id" : "%s"} }%s`, d.GetID(), "\n"))
		data, err := json.Marshal(d)
		if err != nil {
			c.Logger.Errorf("Cannot encode record %d: %s", d.GetID(), err)
			continue
		}
		data = append(data, "\n"...) // <-- Append newline to the data payload

		// Append payloads to the buffer (ignoring write errors)
		buf.Grow(len(meta) + len(data))
		buf.Write(meta)
		buf.Write(data)

		// When a threshold is reached, execute the Bulk() request with body from buffer
		if i > 0 && i%BatchSize == 0 || i == count-1 {
			c.Logger.Infof("> Batch %-2d of %d", currBatch, numBatches)
			res, err = c.EC.Bulk(bytes.NewReader(buf.Bytes()), c.EC.Bulk.WithIndex(indexName), c.EC.Bulk.WithDocumentType("meter"))
			if err != nil {
				c.Logger.Errorf("Failure indexing batch %d: %s", currBatch, err)
			}
			// If the whole request failed, print error and mark all documents as failed
			// A successful response might still contain errors for particular documents
			if res.IsError() {
				numErrors += numItems
				if err := json.NewDecoder(res.Body).Decode(&raw); err != nil {
					c.Logger.Errorf("Failure to to parse response body: %s", err)
				} else {
					c.Logger.Infof("  Error: [%d] %s: %s",
						res.StatusCode,
						raw["error"].(map[string]interface{})["type"],
						raw["error"].(map[string]interface{})["reason"],
					)
				}
			} else {
				if err := json.NewDecoder(res.Body).Decode(&blk); err != nil {
					c.Logger.Errorf("Failure to to parse response body: %s", err)
				} else {
					for _, d := range blk.Items {
						if d.Index.Status > 201 {
							numErrors++
							c.Logger.Infof("  Error: [%d]: %s: %s: %s: %s",
								d.Index.Status,
								d.Index.Error.Type,
								d.Index.Error.Reason,
								d.Index.Error.Cause.Type,
								d.Index.Error.Cause.Reason,
							)
						} else {
							numIndexed++
						}
					}
				}
			}

			// Close the response body, to prevent reaching the limit for goroutines or file handles
			// Reset the buffer and items counter
			_ = res.Body.Close()
			buf.Reset()
			numItems = 0
		}
	}

	// Report the results: number of indexed docs, number of errors, duration, indexing rate
	c.Logger.Infof(strings.Repeat("=", 80))

	dur := time.Since(start)
	if numErrors > 0 {
		c.Logger.Infof(
			"Indexed [%d] documents with [%d] errors in %s (%.0f docs/sec)",
			numIndexed,
			numErrors,
			dur.Truncate(time.Millisecond),
			1000.0/float64(dur/time.Millisecond)*float64(numIndexed),
		)
	} else {
		c.Logger.Infof(
			"Successfully indexed [%d] documents in %s (%.0f docs/sec)",
			numIndexed,
			dur.Truncate(time.Millisecond),
			1000.0/float64(dur/time.Millisecond)*float64(numIndexed),
		)
	}
	return count, numErrors
}

func (c *Client) SearchDocuments(indexName string, terms map[string][]string, size int64, scroll time.Duration) (map[string]interface{}, error) {
	var (
		i      = 0
		begin  = time.Now()
		buf    bytes.Buffer
		err    error
		res    *esapi.Response
		result map[string]interface{}
	)

	filter := make([]interface{}, len(terms))
	for k, v := range terms {
		filter[i] = map[string]interface{}{
			"terms": map[string][]string{
				k: v,
			},
		}
		i++
	}
	query := map[string]interface{}{
		"query": map[string]interface{}{
			"bool": map[string]interface{}{
				"filter": filter,
			},
		},
		"size": size,
	}

	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		c.Logger.Errorf("Error encoding query: %s", err)
		return nil, err
	}

	// Add scroll request, perform the search request and close body in the end
	res, err = c.EC.Search(
		c.EC.Search.WithContext(context.Background()),
		c.EC.Search.WithIndex(indexName),
		c.EC.Search.WithBody(&buf),
		c.EC.Search.WithScroll(scroll),
	)
	if err != nil {
		c.Logger.Infof("Request ES: Latency %.6fs, Error %s", time.Since(begin).Seconds(), err.Error())
		return nil, err
	}
	c.Logger.Infof("Request ES: Code %v, Latency %.6fs", res.StatusCode, time.Since(begin).Seconds())
	defer func() {
		_ = res.Body.Close()
	}()

	// Check the response status and print error information
	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			c.Logger.Errorf("Error parsing the response body: %s", err)
		} else {
			c.Logger.Errorf("[%s] %s: %s",
				res.Status(),
				e["error"].(map[string]interface{})["type"],
				e["error"].(map[string]interface{})["reason"],
			)
		}
	}

	// Print the response status, number of results, and request duration.
	if err := json.NewDecoder(res.Body).Decode(&result); err != nil {
		c.Logger.Infof("Error parsing the response body: %s", err)
	}
	c.Logger.Infof(
		"[%s] %d hits; took: %dms",
		res.Status(),
		int(result["hits"].(map[string]interface{})["total"].(float64)),
		int(result["took"].(float64)),
	)
	c.Logger.Infof(strings.Repeat("=", 37))
	return result, nil
}

func (c *Client) ScrollDocuments(scrollID string, scroll time.Duration) (map[string]interface{}, error) {
	var (
		begin  = time.Now()
		buf    bytes.Buffer
		err    error
		res    *esapi.Response
		result map[string]interface{}
	)

	query := map[string]interface{}{
		"scroll_id": scrollID,
	}
	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		c.Logger.Errorf("Error encoding query: %s", err)
		return nil, err
	}

	// Perform the search request and close body in the end
	res, err = c.EC.Scroll(
		c.EC.Scroll.WithScroll(scroll),
		c.EC.Scroll.WithBody(&buf),
	)
	if err != nil {
		c.Logger.Infof("Request ES: Latency %.6fs, Error %s", time.Since(begin).Seconds(), err.Error())
		return nil, err
	}
	c.Logger.Infof("Request ES: Code %v, Latency %.6fs", res.StatusCode, time.Since(begin).Seconds())
	defer func() {
		_ = res.Body.Close()
	}()

	// Check the response status and print error information
	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			c.Logger.Errorf("Error parsing the response body: %s", err)
		} else {
			c.Logger.Errorf("[%s] %s: %s",
				res.Status(),
				e["error"].(map[string]interface{})["type"],
				e["error"].(map[string]interface{})["reason"],
			)
		}
	}

	// Print the response status, number of results, and request duration.
	if err := json.NewDecoder(res.Body).Decode(&result); err != nil {
		c.Logger.Infof("Error parsing the response body: %s", err)
	}
	c.Logger.Infof(
		"[%s] %d hits; took: %dms",
		res.Status(),
		int(result["hits"].(map[string]interface{})["total"].(float64)),
		int(result["took"].(float64)),
	)
	c.Logger.Infof(strings.Repeat("=", 37))
	return result, nil
}

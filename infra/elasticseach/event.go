package elasticseach

import (
	"bytes"
	"encoding/json"
	"regexp"
	"strings"
	"text/template"
	"time"

	"mars/common"
	"mars/infra/cache"
	"mars/model"
)

func (c *Client) getEventSearchBody(meta *model.EventMeta) string {
	query, ok := querySet[meta.Indicator]
	if !ok {
		c.Logger.Error("Can not find query for %s", meta.Indicator)
		return ""
	}

	t, err := template.New("event_query").Parse(query)
	if err != nil {
		c.Logger.Error("New template error, %s", err.Error())
		return ""
	}
	result := bytes.NewBufferString("")
	if err = t.Execute(result, meta.ToSample()); err != nil {
		return ""
	}
	return result.String()
}

func (c *Client) filterEventResponse(meta *model.EventMeta, response *APISearchResponse) {
	// Filter mismatch events use more accurate regular expressions
	hits := make([]*APISearchHitsEvent, 0)
	validPodName := regexp.MustCompile(meta.PodName)
	validRSName := regexp.MustCompile(meta.Name + "-[a-z0-9]{7,10}")
	for _, i := range response.Hits.Hits {
		event := i.Source.Detail.Event
		if event.InvolvedObject.Kind == "Pod" {
			if validPodName.MatchString(event.InvolvedObject.Name) {
				hits = append(hits, i)
				continue
			}
		}
		if event.InvolvedObject.Kind == "ReplicaSet" {
			if validRSName.MatchString(event.InvolvedObject.Name) {
				hits = append(hits, i)
				continue
			}
		}
		if common.StringInSlice(event.InvolvedObject.Kind, []string{"Deployment", "DaemonSet", "StatefulSet"}) {
			if meta.Name == event.InvolvedObject.Name {
				hits = append(hits, i)
			}
		}
	}
	response.Hits.Hits = hits
}

func (c *Client) parseEventSearchResponse(meta *model.EventMeta, response *APISearchResponse) float64 {
	// Init namespaced events cache and send a heartbeat
	c.filterEventResponse(meta, response)
	uniqueName := meta.Cluster + "___" + meta.Namespace
	nc := cache.EC.Get(uniqueName)
	if nc == nil {
		c.Logger.Infof("Can not find namespaced events cache %s, set it", uniqueName)
		nc = cache.NewNamespacedEventsCache()
		cache.EC.Set(uniqueName, nc)
	} else {
		nc.Get("heart_beat")
	}
	// Traverse the hits list add count event reasons
	var counter float64
	c.Logger.Infof("Get %d events from es", len(response.Hits.Hits))
	for _, i := range response.Hits.Hits {
		current := i.Source.Detail.Event
		nc := cache.EC.Get(uniqueName)
		if nc == nil {
			c.Logger.Infof("Can not find namespaced events cache %s, set it", uniqueName)
			nc = cache.NewNamespacedEventsCache()
			cache.EC.Set(uniqueName, nc)
		}
		previous := nc.Get(string(current.UID))
		if previous == nil {
			if current.FirstTimestamp.Time.After(nc.LastTransitionTime()) {
				counter += float64(current.Count)
			}
		} else {
			if previous.Count < current.Count {
				counter += float64(current.Count - previous.Count)
			}
		}
		if previous == nil || previous.Count < current.Count {
			c.Logger.Infof("Find a new event %s/%s, cache it", current.Namespace, current.Name)
			nc.Set(current)
		}
	}
	return counter
}

func (c *Client) GetEventCount(meta *model.EventMeta) float64 {
	begin := time.Now()
	timestamp := meta.Start.UTC().Unix() * 1000000
	index := "event-" + time.Unix(timestamp/1000000, timestamp%1000000).Format("20060102")
	body := c.getEventSearchBody(meta)
	c.Logger.Infof("Request ES: Index %s, Body \n%s", index, body)
	res, err := c.EC.Search(
		c.EC.Search.WithIndex(index),
		c.EC.Search.WithBody(strings.NewReader(body)),
	)
	if err != nil {
		c.Logger.Infof("Request ES: Latency %.6fs, Error %s", time.Since(begin).Seconds(), err.Error())
		return 0
	}
	c.Logger.Infof("Request ES: Code %v, Latency %.6fs", res.StatusCode, time.Since(begin).Seconds())
	defer res.Body.Close()

	// Check the response status and print error information
	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			c.Logger.Errorf("Error parsing the response body: %s", err.Error())
		} else {
			c.Logger.Errorf("ES response error: type %s, reason %s",
				e["error"].(map[string]interface{})["type"],
				e["error"].(map[string]interface{})["reason"],
			)
		}
		return 0
	}

	// Print the response status, number of results
	var response APISearchResponse
	if err := json.NewDecoder(res.Body).Decode(&response); err != nil {
		c.Logger.Errorf("Error parsing the response body: %s", err)
		return 0
	}
	return c.parseEventSearchResponse(meta, &response)
}

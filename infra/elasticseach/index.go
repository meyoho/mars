package elasticseach

import (
	"fmt"
	"net/http"
)

func (c *Client) CreateIndex(indexName string) error {
	res, err := c.EC.Indices.Get([]string{indexName})
	if err != nil {
		return err
	}
	if !res.IsError() {
		return nil
	}
	if res.IsError() && res.StatusCode != http.StatusNotFound {
		return fmt.Errorf("get index %s error, api response %v", indexName, res)
	}
	res, err = c.EC.Indices.Create(indexName)
	if err != nil {
		return err
	}
	if res.IsError() {
		return fmt.Errorf("create index %s error, api response %v", indexName, res)
	}
	return nil
}

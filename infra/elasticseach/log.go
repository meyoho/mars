package elasticseach

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"text/template"
	"time"

	"mars/model"
)

func (c *Client) getLogCountBody(meta *model.LogMeta) string {
	query, ok := querySet[meta.Indicator]
	if !ok {
		panic(fmt.Sprintf("Can not find query for %s", meta.Indicator))
	}

	t, err := template.New("log_query").Parse(query)
	if err != nil {
		return ""
	}

	result := bytes.NewBufferString("")
	if err = t.Execute(result, meta.ToSample()); err != nil {
		return ""
	}
	return result.String()
}

func (c *Client) GetLogCount(meta *model.LogMeta) float64 {
	begin := time.Now()
	timestamp := meta.Start.UTC().Unix() * 1000000
	index := "log-" + time.Unix(timestamp/1000000, timestamp%1000000).Format("20060102")
	body := c.getLogCountBody(meta)
	c.Logger.Infof("Request ES: Index %s, Body \n%s", index, body)
	res, err := c.EC.Count(
		c.EC.Count.WithIndex(index),
		c.EC.Count.WithBody(strings.NewReader(body)),
	)
	if err != nil {
		c.Logger.Infof("Request ES: Latency %.6fs, Error %s", time.Since(begin).Seconds(), err.Error())
		return 0
	}
	c.Logger.Infof("Request ES: Code %v, Latency %.6fs", res.StatusCode, time.Since(begin).Seconds())
	defer res.Body.Close()

	// Check the response status and print error information
	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			c.Logger.Errorf("Error parsing the response body: %s", err.Error())
		} else {
			c.Logger.Errorf("ES response error: type %s, reason %s",
				e["error"].(map[string]interface{})["type"],
				e["error"].(map[string]interface{})["reason"],
			)
		}
		return 0
	}

	// Print the response status, number of results
	var r map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		c.Logger.Errorf("Error parsing the response body: %s", err)
		return 0
	}
	return float64(r["count"].(float64))
}

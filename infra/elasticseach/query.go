package elasticseach

import (
	"strings"

	"mars/model"
)

var querySet = map[string]string{
	model.WorkloadLogKeywordCount:
	`{
		"query": {
			"bool": {
				"filter": [
					{
						"terms": {
							"region_name.keyword": [
								"{{.Cluster}}"
							]
						}
					},
					{
						"terms": {
							"kubernetes_namespace.keyword": [
								"{{.Namespace}}"
							]
						}
					},
					{
						"regexp": {
							"pod_name.keyword": "{{.PodName}}"
						}
					},
					{
						"match": {
							"log_data": {
								"operator": "and",
								"query": "{{.Query}}"
							}
						}
					},
					{
						"range": {
							"@timestamp": {
								"lt": "{{.End}}",
								"gte": "{{.Start}}"
							}
						}
					}
				]
			}
		}
	}`,

	model.WorkloadEventReasonCount:
	`{
		"query": {
			"bool": {
				"filter": [
					{
						"terms": {
							"detail.cluster_name.keyword": [
								"{{.Cluster}}"
							]
						}
					},
					{
						"terms": {
							"detail.event.involvedObject.namespace": [
								"{{.Namespace}}"
							]
						}
					},
					{
						"terms": {
							"detail.event.involvedObject.kind": [
								{{range $i,$a := .Kind}}{{if gt $i 0 }}, {{end}}"{{.}}"{{end}}
							]
						}
					},
					{
						"regexp": {
							"detail.event.involvedObject.name": "({{.Name}})|({{.Name}}-).*"
						}
					},
					{
						"terms": {
							"detail.event.reason.keyword": [
								{{range $i,$a := .Query}}{{if gt $i 0 }}, {{end}}"{{.}}"{{end}}
							]
						}
					},
					{
						"range": {
							"time": {
								"gte": "{{.Start}}"
							}
						}
					}
				]
			}
		}
	}`,
}

func init() {
	for key, value := range querySet {
		value = strings.Replace(value, "\n\t", "\n", -1)
		querySet[key] = value
	}
}

package kubernetes

import (
	"sync"

	"mars/common"
	"mars/model"

	"k8s.io/client-go/dynamic"
)

type ClusterWatcher struct {
	// watchedResources include all resource watchers for this cluster
	watchedResources map[string]*ResourceWatcher

	// rwMutex is a read&write lock for watched resources
	rwMutex sync.RWMutex

	// client is a dynamic interface which will be used to watch resources
	client dynamic.Interface

	// stopChannel will be used to pass signal to goroutines
	stopChannel chan struct{}

	// stopped show the status of this watcher
	stopped bool

	// cluster include all cluster meta info
	cluster *model.Cluster

	// Logger handler
	Logger *common.Logger
}

func NewClusterWatcher(c *model.Cluster) *ClusterWatcher {
	rc, err := NewResourceClient(c)
	if err != nil {
		panic(err.Error())
	}
	return &ClusterWatcher{
		cluster:          c,
		client:           rc.Client(),
		Logger:           rc.Logger(),
		stopChannel:      make(chan struct{}),
		watchedResources: make(map[string]*ResourceWatcher, 0),
	}
}

// StartResourceWatcher start to watch a single resource
func (c *ClusterWatcher) StartResourceWatcher(rc *ResourceConfig) {
	newRWatcher := func() *ResourceWatcher {
		c.rwMutex.Lock()
		defer c.rwMutex.Unlock()

		if c.isResourceWatched(rc.GroupVersionKind.Kind) {
			c.Logger.Infof("%s has been watched!", rc.GroupVersionKind.Kind)
			return nil
		}

		watcher := newResourceWatcher(c, rc)
		if watcher != nil {
			c.addResourceWatcher(watcher)
		}
		return watcher
	}

	go func() {
		rw := newRWatcher()
		if rw == nil {
			return
		}
		if err := rw.run(); err != nil {
			func() {
				c.rwMutex.Lock()
				defer c.rwMutex.Unlock()
				c.removeResourceWatcher(rc.GroupVersionKind.Kind)
			}()
			return
		}
	}()
}

// StopResourceWatcher will stop a specified resource watcher in this cluster
func (c *ClusterWatcher) StopResourceWatcher(resource string) {
	c.rwMutex.Lock()
	defer c.rwMutex.Unlock()

	if c.isResourceWatched(resource) {
		return
	}

	rw, ok := c.watchedResources[resource]
	if ok {
		rw.stop()
		c.removeResourceWatcher(resource)
	}
}

// Stop will stop all resource watcher in this cluster
func (c *ClusterWatcher) Stop() {
	c.rwMutex.Lock()
	defer c.rwMutex.Unlock()

	for r, w := range c.watchedResources {
		c.Logger.Infof("Stop watch resource '%s'", r)
		w.stop()
	}
	c.watchedResources = make(map[string]*ResourceWatcher)
	c.stopped = true
}

// isResourceWatched return true if the specific resource is already watched
func (c *ClusterWatcher) isResourceWatched(resource string) (watched bool) {
	_, watched = c.watchedResources[resource]
	return
}

// addResourceWatcher add a resource watcher to watched set
func (c *ClusterWatcher) addResourceWatcher(rw *ResourceWatcher) {
	c.watchedResources[rw.resourceConfig.GroupVersionKind.Kind] = rw
}

// removeResourceWatcher remove a resource watcher from watched set
func (c *ClusterWatcher) removeResourceWatcher(resource string) {
	delete(c.watchedResources, resource)
}

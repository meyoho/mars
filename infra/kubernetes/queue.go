package kubernetes

import (
	"fmt"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/tools/cache"
)

type ProxyQueue struct {
	resynced bool
	lw       cache.ListerWatcher
	h        ResourceEventHandler
}

func NewProxyQueue(h ResourceEventHandler, lw cache.ListerWatcher) *ProxyQueue {
	return &ProxyQueue{
		resynced: false,
		lw:       lw,
		h:        h,
	}
}

func (p *ProxyQueue) Add(obj interface{}) error {
	if p.h != nil {
		p.h.OnAdd(obj)
	}
	return nil
}

func (p *ProxyQueue) Update(obj interface{}) error {
	if p.h != nil {
		p.h.OnUpdate(nil, obj)
	}
	return nil
}

func (p *ProxyQueue) Delete(obj interface{}) error {
	if p.h != nil {
		p.h.OnDelete(obj)
	}
	return nil
}

func (p *ProxyQueue) List() []interface{} {
	return nil
}

func (p *ProxyQueue) ListKeys() []string {
	return nil
}

func (p *ProxyQueue) Get(obj interface{}) (item interface{}, exists bool, err error) {
	return nil, false, nil
}

func (p *ProxyQueue) GetByKey(key string) (item interface{}, exists bool, err error) {
	return nil, false, nil
}

func (p *ProxyQueue) Replace(objs []interface{}, resourceversion string) error {
	if p.h != nil {
		p.h.OnReplace(objs)
	}
	p.resynced = true
	return nil
}

func (p *ProxyQueue) Resync() error {
	if p.h != nil {
		object, err := p.lw.List(metaV1.ListOptions{})
		if err != nil {
			return err
		}
		resourceList, ok := object.(*unstructured.UnstructuredList)
		if !ok {
			return fmt.Errorf("can not translate runtime object to resource list")
		}
		objects := make([]interface{}, len(resourceList.Items))
		for index, item := range resourceList.Items {
			objects[index] = item.DeepCopy()
		}
		p.h.OnReplace(objects)
	}
	p.resynced = true
	return nil
}

func (p *ProxyQueue) Pop(pop cache.PopProcessFunc) (interface{}, error) {
	return nil, nil
}

func (p *ProxyQueue) AddIfNotPresent(interface{}) error {
	return nil
}

// Return true if the first batch of itemp has been popped
func (p *ProxyQueue) HasSynced() bool {
	return p.resynced
}

// Close queue
func (p *ProxyQueue) Close() { /* Nothing to close */ }

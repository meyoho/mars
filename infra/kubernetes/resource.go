package kubernetes

import (
	"time"

	"mars/common"
	"mars/config"
	"mars/model"

	"github.com/juju/errors"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
)

type ResourceClientInterface interface {
	Client() dynamic.Interface
	Logger() *common.Logger
}

type ResourceClient struct {
	client dynamic.Interface
	logger *common.Logger
}

func (c *ResourceClient) Client() dynamic.Interface {
	return c.client
}

func (c *ResourceClient) Logger() *common.Logger {
	return c.logger
}

func NewResourceClient(c *model.Cluster) (ResourceClientInterface, error) {
	cfg := &rest.Config{
		Host:        c.KubernetesConfig.Endpoint,
		BearerToken: c.KubernetesConfig.Token,
		Timeout:     time.Duration(config.GlobalConfig.Kubernetes.WatchTimeout) * time.Second,
	}
	cfg.Insecure = true
	client, err := dynamic.NewForConfig(cfg)
	if err != nil {
		return nil, errors.Annotatef(err, "new dynamic client with cfg %+v of %s error", cfg, c.UUID)
	}

	return &ResourceClient{
		client: client,
		logger: common.NewLogger(map[string]string{"c": c.Name, "r": "cw"}),
	}, nil
}

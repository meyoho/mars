package kubernetes

import (
	"fmt"
	"time"

	"mars/common"
	"mars/config"

	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/tools/cache"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	utilRuntime "k8s.io/apimachinery/pkg/util/runtime"
)

type ResourceConfig struct {
	GroupVersionKind schema.GroupVersionKind
	RuntimeObject    runtime.Object
	EventsHandlers   ResourceEventHandlerFuncs
}

type ResourceWatcher struct {
	// clusterWatcher is the parent of this resource watcher
	clusterWatcher *ClusterWatcher

	// resourceConfig include all meta info for this resource
	resourceConfig *ResourceConfig

	// stopChannel will be used to pass signal to goroutines
	stopChannel chan struct{}

	// stopped show the status of this watcher
	stopped bool

	// Logger handler
	Logger *common.Logger
}

func newResourceWatcher(cw *ClusterWatcher, rc *ResourceConfig) *ResourceWatcher {
	return &ResourceWatcher{
		resourceConfig: rc,
		clusterWatcher: cw,
		Logger:         cw.Logger,
		stopChannel:    make(chan struct{}),
		stopped:        true,
	}
}

func (r *ResourceWatcher) stop() {
	if r.stopped {
		return
	}
	r.closeStopChannel()
	r.Logger.Info("Resource watcher has been stopped")
	r.stopChannel = make(chan struct{})
	r.stopped = true
}

func (r *ResourceWatcher) run() error {
	if !r.stopped {
		return nil
	}
	defer utilRuntime.HandleCrash()

	getListWatch := func() *cache.ListWatch {
		re := r.clusterWatcher.client.Resource(schema.GroupVersionResource{
			Group:    r.resourceConfig.GroupVersionKind.Group,
			Version:  r.resourceConfig.GroupVersionKind.Version,
			Resource: common.GetKubernetesTypeFromKind(r.resourceConfig.GroupVersionKind.Kind),
		}).Namespace("")

		return &cache.ListWatch{
			ListFunc: func(options metaV1.ListOptions) (runtime.Object, error) {
				options.FieldSelector = fields.Everything().String()
				return re.List(options)
			},
			WatchFunc: func(options metaV1.ListOptions) (watch.Interface, error) {
				options.Watch = true
				options.FieldSelector = fields.Everything().String()
				return re.Watch(options)
			},
		}
	}

	getEventHandler := func() ResourceEventHandlerFuncs {
		return ResourceEventHandlerFuncs{
			ResourceEventHandlerFuncs: cache.ResourceEventHandlerFuncs{
				AddFunc: func(obj interface{}) {
					r.resourceConfig.EventsHandlers.AddFunc(obj)
				},
				UpdateFunc: func(old, new interface{}) {
					r.resourceConfig.EventsHandlers.UpdateFunc(old, new)
				},
				DeleteFunc: func(obj interface{}) {
					r.resourceConfig.EventsHandlers.DeleteFunc(obj)
				},
			},
			ReplaceFunc: func(objs []interface{}) {
				r.resourceConfig.EventsHandlers.ReplaceFunc(objs)
			},
		}
	}

	r.Logger.Infof("Watching resource with api version '%s', kind '%s'", r.resourceConfig.GroupVersionKind.GroupVersion(), r.resourceConfig.GroupVersionKind.Kind)
	informer := NewInformer(
		fmt.Sprintf("%s_%s", r.clusterWatcher.cluster.Name, r.resourceConfig.GroupVersionKind.Kind),
		getListWatch(),
		r.resourceConfig.RuntimeObject,
		time.Second * time.Duration(config.GlobalConfig.Kubernetes.ResyncPeriod),
		getEventHandler(),
	)
	go informer.Run(r.stopChannel)

	if !cache.WaitForCacheSync(r.stopChannel, informer.HasSynced) {
		err := fmt.Errorf("timed out waiting for caches to sync")
		utilRuntime.HandleError(err)
		r.Logger.Errorf("Resource watcher error when sync: %s", err)
		r.closeStopChannel()
		return err
	}
	r.Logger.Infof("Resource watcher informer synced and ready")
	r.stopped = false
	return nil
}

func (r *ResourceWatcher) closeStopChannel() {
	defer func() {
		if recover() != nil {
			return
		}
	}()

	close(r.stopChannel)
}

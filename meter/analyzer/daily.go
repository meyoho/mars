package analyzer

import (
	"encoding/json"
	"time"

	"mars/common"
	"mars/infra/elasticseach"
	mc "mars/meter/common"
	"mars/model"
)

type DailyAnalyzer struct {
	startTime                    time.Time
	endTime                      time.Time
	namespacePodUsageSummary     []*model.ResourceUsageSummaryRecord
	namespaceQuotaSummary        []*model.ResourceUsageSummaryRecord
	projectPodUsageSummary       []*model.ResourceUsageSummaryRecord
	projectNamespaceQuotaSummary []*model.ResourceUsageSummaryRecord
	projectQuotaSummary          []*model.ResourceUsageSummaryRecord
	Logger                       *common.Logger
}

func NewDailyAnalyzer(start time.Time, end time.Time) *DailyAnalyzer {
	return &DailyAnalyzer{
		startTime:                    start,
		endTime:                      end,
		namespacePodUsageSummary:     nil,
		namespaceQuotaSummary:        nil,
		projectPodUsageSummary:       nil,
		projectNamespaceQuotaSummary: nil,
		projectQuotaSummary:          nil,
		Logger:                       common.NewMeterLogger(map[string]string{"r": "daily_summary"}),
	}
}

func (s *DailyAnalyzer) Run() {
	// Initialize summary records array
	s.namespacePodUsageSummary = []*model.ResourceUsageSummaryRecord{}
	s.namespaceQuotaSummary = []*model.ResourceUsageSummaryRecord{}
	s.projectPodUsageSummary = []*model.ResourceUsageSummaryRecord{}
	s.projectNamespaceQuotaSummary = []*model.ResourceUsageSummaryRecord{}
	s.projectQuotaSummary = []*model.ResourceUsageSummaryRecord{}

	// Search documents in detail index and summary
	index := mc.GetDetailIndexName(s.startTime)
	client := elasticseach.NewClient(s.Logger)
	var (
		err    error
		result map[string]interface{}
		scroll       = time.Minute * 5
		size   int64 = 100
		terms        = map[string][]string{"day.keyword": {s.startTime.Format("20060102")}}
	)

	// Initialize documents search and scroll
	result, err = client.SearchDocuments(index, terms, size, scroll)
	if err != nil {
		s.Logger.Errorf("Search index %s failed with %s,%s,%s, error %s", index, terms, size, scroll, err.Error())
		return
	}
	scrollID := result["_scroll_id"].(string)
	for {
		hits := result["hits"].(map[string]interface{})["hits"].([]interface{})
		if len(hits) == 0 {
			break
		}
		s.addHits(hits)
		result, err = client.ScrollDocuments(scrollID, scroll)
		if err != nil {
			s.Logger.Errorf("Scroll index %s failed with %s,%s, error %s", index, scroll, scrollID, err.Error())
			break
		}
	}

	// Add empty records if needed and save summary records to summary index
	s.addEmptyRecordsForProject()
	s.addEmptyRecordsForNamespace()
	if err := s.save(); err != nil {
		s.Logger.Errorf("Save to index %s, error %s", index, err.Error())
	}
}

func (s *DailyAnalyzer) addHits(hits []interface{}) {
	for _, item := range hits {
		record := &model.ResourceUsageRecord{}
		source := item.(map[string]interface{})["_source"]
		sourceBytes, err := json.Marshal(source)
		if err != nil {
			s.Logger.Errorf("Marshal source to bytes, error %s", err.Error())
			break
		}
		if err := json.Unmarshal(sourceBytes, record); err != nil {
			s.Logger.Errorf("Unmarshal bytes to record, error %s", err.Error())
			break
		}
		r := model.ResourceUsageSummaryRecord{ResourceUsageRecord: *record, Period: mc.PeriodDaily}
		r.StartTime = s.startTime
		r.EndTime = s.endTime
		switch record.Type {
		case mc.ProjectQuota:
			s.projectQuotaSummary = append(s.projectQuotaSummary, &r)
		case mc.NamespaceQuota:
			s.addNamespaceQuotaRecord(&r)
		case mc.PodUsage:
			s.addPodUsageRecord(&r)
		}
	}
}

func (s *DailyAnalyzer) addNamespaceQuotaRecord(record *model.ResourceUsageSummaryRecord) {
	if record.Type != mc.NamespaceQuota {
		return
	}

	// Add namespace quota record to namespace summary
	if record.Cluster != "" {
		mirror := record.Clone()
		s.namespaceQuotaSummary = append(s.namespaceQuotaSummary, mirror)
	}

	// Add namespace quota record to project summary
	if record.Project != "" {
		find := false
		mirror := record.Clone()
		for _, i := range s.projectNamespaceQuotaSummary {
			if i.Project == mirror.Project {
				i.Meter.Add(mirror.Meter)
				find = true
				break
			}
		}
		if !find {
			mirror.Cluster = ""
			mirror.Namespace = ""
			s.projectNamespaceQuotaSummary = append(s.projectNamespaceQuotaSummary, mirror)
		}
	}
}

func (s *DailyAnalyzer) addPodUsageRecord(record *model.ResourceUsageSummaryRecord) {
	if record.Type != mc.PodUsage {
		return
	}

	// Add pod usage record to namespace summary
	if record.Cluster != "" {
		find := false
		mirror := record.Clone()
		for _, i := range s.namespacePodUsageSummary {
			if i.Project == mirror.Project && i.Cluster == mirror.Cluster && i.Namespace == mirror.Namespace {
				i.Meter.Add(mirror.Meter)
				find = true
				break
			}
		}
		if !find {
			mirror.Pod = ""
			s.namespacePodUsageSummary = append(s.namespacePodUsageSummary, mirror)
		}
	}

	// Add pod usage record to project summary
	if record.Project != "" {
		find := false
		mirror := record.Clone()
		for _, i := range s.projectPodUsageSummary {
			if i.Project == mirror.Project {
				i.Meter.Add(mirror.Meter)
				find = true
				break
			}
		}
		if !find {
			mirror.Cluster = ""
			mirror.Namespace = ""
			mirror.Pod = ""
			s.projectPodUsageSummary = append(s.projectPodUsageSummary, mirror)
		}
	}
}

func (s *DailyAnalyzer) save() error {
	index := mc.GetSummaryIndexName(s.startTime)
	client := elasticseach.NewClient(s.Logger)
	if err := client.CreateIndex(index); err != nil {
		s.Logger.Errorf("Create index %s failed, error %s", index, err.Error())
		return err
	}

	insert := func(records []*model.ResourceUsageSummaryRecord) {
		if len(records) < 1 {
			s.Logger.Infof("Has no summary records for this kind, skip")
			return
		}
		documents := make([]elasticseach.Document, len(records))
		for i, v := range records {
			mc.SetIDForDailySummaryRecord(v)
			documents[i] = v
			if common.LogLevel > 7 {
				result, _ := json.MarshalIndent(v, "", "   ")
				if v.Cluster == "" {
					s.Logger.Infof("Project %s, %s\n%s", v.Type, v.Project, string(result))
				} else {
					s.Logger.Infof("Namespace %s, %s/%s/%s\n%s", v.Type, v.Project, v.Cluster, v.Namespace, string(result))
				}
			}
		}
		numTotal, numErrors := client.InsertDocuments(index, documents)
		s.Logger.Infof("Insert %d summary records to ElasticSearch, failed %d", numTotal, numErrors)
	}
	insert(s.namespacePodUsageSummary)
	insert(s.namespaceQuotaSummary)
	insert(s.projectPodUsageSummary)
	insert(s.projectNamespaceQuotaSummary)
	insert(s.projectQuotaSummary)
	return nil
}

func (s *DailyAnalyzer) addEmptyRecordsForProject() {
	var (
		find   = false
		mirror *model.ResourceUsageSummaryRecord
		toAdd  []*model.ResourceUsageSummaryRecord
	)

	// Add pod usage records for project quotas
	toAdd = make([]*model.ResourceUsageSummaryRecord, 0)
	for _, i := range s.projectQuotaSummary {
		find = false
		for _, j := range s.projectPodUsageSummary {
			if i.Project == j.Project {
				find = true
				break
			}
		}
		if !find {
			mirror = i.Clone()
			mirror.Meter = &model.Meter{}
			mirror.Type = mc.PodUsage
			toAdd = append(toAdd, mirror)
		}
	}
	s.projectPodUsageSummary = append(s.projectPodUsageSummary, toAdd...)

	// Add pod usage records for namespace quotas
	toAdd = make([]*model.ResourceUsageSummaryRecord, 0)
	for _, i := range s.projectNamespaceQuotaSummary {
		find = false
		for _, j := range s.projectPodUsageSummary {
			if i.Project == j.Project {
				find = true
				break
			}
		}
		if !find {
			mirror = i.Clone()
			mirror.Meter = &model.Meter{}
			mirror.Type = mc.PodUsage
			toAdd = append(toAdd, mirror)
		}
	}
	s.projectPodUsageSummary = append(s.projectPodUsageSummary, toAdd...)

	// Add project quota records for pod usages
	toAdd = make([]*model.ResourceUsageSummaryRecord, 0)
	for _, i := range s.projectPodUsageSummary {
		find = false
		for _, j := range s.projectQuotaSummary {
			if i.Project == j.Project {
				find = true
				break
			}
		}
		if !find {
			mirror = i.Clone()
			mirror.Meter = &model.Meter{}
			mirror.Type = mc.ProjectQuota
			toAdd = append(toAdd, mirror)
		}
	}
	s.projectQuotaSummary = append(s.projectQuotaSummary, toAdd...)

	// Add project namespaces quota records for pod usages
	toAdd = make([]*model.ResourceUsageSummaryRecord, 0)
	for _, i := range s.projectPodUsageSummary {
		find = false
		for _, j := range s.projectNamespaceQuotaSummary {
			if i.Project == j.Project {
				find = true
				break
			}
		}
		if !find {
			mirror = i.Clone()
			mirror.Meter = &model.Meter{}
			mirror.Type = mc.NamespaceQuota
			toAdd = append(toAdd, mirror)
		}
	}
	s.projectNamespaceQuotaSummary = append(s.projectNamespaceQuotaSummary, toAdd...)
}

func (s *DailyAnalyzer) addEmptyRecordsForNamespace() {
	var (
		find   = false
		mirror *model.ResourceUsageSummaryRecord
		toAdd  []*model.ResourceUsageSummaryRecord
	)

	// Add pod usage records for namespace quotas
	toAdd = make([]*model.ResourceUsageSummaryRecord, 0)
	for _, i := range s.namespaceQuotaSummary {
		find = false
		for _, j := range s.namespacePodUsageSummary {
			if i.Project == j.Project && i.Cluster == j.Cluster && i.Namespace == j.Namespace {
				find = true
				break
			}
		}
		if !find {
			mirror = i.Clone()
			mirror.Meter = &model.Meter{}
			mirror.Type = mc.PodUsage
			toAdd = append(toAdd, mirror)
		}
	}
	s.namespacePodUsageSummary = append(s.namespacePodUsageSummary, toAdd...)

	// Add namespace quotas records for pod usage
	toAdd = make([]*model.ResourceUsageSummaryRecord, 0)
	for _, i := range s.namespacePodUsageSummary {
		find = false
		for _, j := range s.namespaceQuotaSummary {
			if i.Project == j.Project && i.Cluster == j.Cluster && i.Namespace == j.Namespace {
				find = true
				break
			}
		}
		if !find {
			mirror = i.Clone()
			mirror.Meter = &model.Meter{}
			mirror.Type = mc.NamespaceQuota
			toAdd = append(toAdd, mirror)
		}
	}
	s.namespaceQuotaSummary = append(s.namespaceQuotaSummary, toAdd...)
}

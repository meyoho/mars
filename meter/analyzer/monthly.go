package analyzer

import (
	"encoding/json"
	"time"

	"mars/common"
	"mars/infra/elasticseach"
	mc "mars/meter/common"
	"mars/model"
)

type MonthlyAnalyzer struct {
	startTime                    time.Time
	endTime                      time.Time
	namespacePodUsageSummary     []*model.ResourceUsageSummaryRecord
	namespaceQuotaSummary        []*model.ResourceUsageSummaryRecord
	projectPodUsageSummary       []*model.ResourceUsageSummaryRecord
	projectNamespaceQuotaSummary []*model.ResourceUsageSummaryRecord
	projectQuotaSummary          []*model.ResourceUsageSummaryRecord
	Logger                       *common.Logger
}

func NewMonthlyAnalyzer(start time.Time, end time.Time) *MonthlyAnalyzer {
	return &MonthlyAnalyzer{
		startTime:                    start,
		endTime:                      end,
		namespacePodUsageSummary:     nil,
		namespaceQuotaSummary:        nil,
		projectPodUsageSummary:       nil,
		projectNamespaceQuotaSummary: nil,
		projectQuotaSummary:          nil,
		Logger:                       common.NewMeterLogger(map[string]string{"r": "monthly_summary"}),
	}
}

func (s *MonthlyAnalyzer) Run() {
	// Initialize summary records array
	s.namespacePodUsageSummary = []*model.ResourceUsageSummaryRecord{}
	s.namespaceQuotaSummary = []*model.ResourceUsageSummaryRecord{}
	s.projectPodUsageSummary = []*model.ResourceUsageSummaryRecord{}
	s.projectNamespaceQuotaSummary = []*model.ResourceUsageSummaryRecord{}
	s.projectQuotaSummary = []*model.ResourceUsageSummaryRecord{}

	// Search documents in summary index and summary
	index := mc.GetSummaryIndexName(s.startTime)
	client := elasticseach.NewClient(s.Logger)
	var (
		err    error
		result map[string]interface{}
		scroll       = time.Minute * 5
		size   int64 = 100
		terms        = map[string][]string{"period.keyword": {mc.PeriodDaily}}
	)

	// Initialize documents search and scroll
	result, err = client.SearchDocuments(index, terms, size, scroll)
	if err != nil {
		s.Logger.Errorf("Search index %s failed with %s,%s,%s, error %s", index, terms, size, scroll, err.Error())
		return
	}
	scrollID := result["_scroll_id"].(string)
	for {
		hits := result["hits"].(map[string]interface{})["hits"].([]interface{})
		if len(hits) == 0 {
			break
		}
		s.addHits(hits)
		result, err = client.ScrollDocuments(scrollID, scroll)
		if err != nil {
			s.Logger.Errorf("Scroll index %s failed with %s,%s, error %s", index, scroll, scrollID, err.Error())
			break
		}
	}

	// Save summary records to summary index
	if err := s.save(); err != nil {
		s.Logger.Errorf("Save to index %s, error %s", index, err.Error())
	}
}

func (s *MonthlyAnalyzer) addHits(hits []interface{}) {
	for _, item := range hits {
		record := &model.ResourceUsageSummaryRecord{}
		source := item.(map[string]interface{})["_source"]
		sourceBytes, err := json.Marshal(source)
		if err != nil {
			s.Logger.Errorf("Marshal source to bytes, error %s", err.Error())
			break
		}
		if err := json.Unmarshal(sourceBytes, record); err != nil {
			s.Logger.Errorf("Unmarshal bytes to record, error %s", err.Error())
			break
		}
		switch record.Type {
		case mc.ProjectQuota:
			s.addProjectQuotaRecord(record)
		case mc.NamespaceQuota:
			s.addNamespaceQuotaRecord(record)
		case mc.PodUsage:
			s.addPodUsageRecord(record)
		}
	}
}

func (s *MonthlyAnalyzer) addProjectQuotaRecord(record *model.ResourceUsageSummaryRecord) {
	if record.Type != mc.ProjectQuota {
		return
	}

	// Add project quota record to project summary
	if record.Project == "" {
		return
	}
	find := false
	mirror := record.Clone()
	mirror.Period = mc.PeriodMonthly
	mirror.Day = ""
	mirror.Month = s.startTime.Format("200601")
	for _, i := range s.projectQuotaSummary {
		if i.Project == mirror.Project {
			i.Add(mirror)
			find = true
			break
		}
	}
	if !find {
		mirror.Cluster = ""
		mirror.Namespace = ""
		s.projectQuotaSummary = append(s.projectQuotaSummary, mirror)
	}
}

func (s *MonthlyAnalyzer) addNamespaceQuotaRecord(record *model.ResourceUsageSummaryRecord) {
	if record.Type != mc.NamespaceQuota {
		return
	}

	// Add namespace quota record to namespace summary
	if record.Cluster != "" {
		mirror := record.Clone()
		mirror.Period = mc.PeriodMonthly
		mirror.Day = ""
		mirror.Month = s.startTime.Format("200601")
		find := false
		for _, i := range s.namespaceQuotaSummary {
			if i.Project == mirror.Project && i.Cluster == mirror.Cluster && i.Namespace == mirror.Namespace {
				i.Add(mirror)
				find = true
				break
			}
		}
		if !find {
			s.namespaceQuotaSummary = append(s.namespaceQuotaSummary, mirror)
		}
	}

	// Add namespace quota record to project summary
	if record.Project != "" {
		// Skip daily summary for project
		if record.Cluster == "" {
			return
		}
		find := false
		mirror := record.Clone()
		mirror.Period = mc.PeriodMonthly
		mirror.Day = ""
		mirror.Month = s.startTime.Format("200601")
		for _, i := range s.projectNamespaceQuotaSummary {
			if i.Project == mirror.Project {
				i.Add(mirror)
				find = true
				break
			}
		}
		if !find {
			mirror.Cluster = ""
			mirror.Namespace = ""
			s.projectNamespaceQuotaSummary = append(s.projectNamespaceQuotaSummary, mirror)
		}
	}

}

func (s *MonthlyAnalyzer) addPodUsageRecord(record *model.ResourceUsageSummaryRecord) {
	if record.Type != mc.PodUsage {
		return
	}

	// Add pod usage record to namespace summary
	if record.Cluster != "" {
		mirror := record.Clone()
		mirror.Period = mc.PeriodMonthly
		mirror.Day = ""
		mirror.Month = s.startTime.Format("200601")
		find := false
		for _, i := range s.namespacePodUsageSummary {
			if i.Project == mirror.Project && i.Cluster == mirror.Cluster && i.Namespace == mirror.Namespace {
				i.Meter.Add(mirror.Meter)
				find = true
				break
			}
		}
		if !find {
			s.namespacePodUsageSummary = append(s.namespacePodUsageSummary, mirror)
		}
	}

	// Add pod usage record to project summary
	if record.Project != "" {
		// Skip daily summary for project
		if record.Cluster == "" {
			return
		}
		find := false
		mirror := record.Clone()
		mirror.Period = mc.PeriodMonthly
		mirror.Day = ""
		mirror.Month = s.startTime.Format("200601")
		for _, i := range s.projectPodUsageSummary {
			if i.Project == mirror.Project {
				i.Meter.Add(mirror.Meter)
				find = true
				break
			}
		}
		if !find {
			mirror.Cluster = ""
			mirror.Namespace = ""
			s.projectPodUsageSummary = append(s.projectPodUsageSummary, mirror)
		}
	}
}

func (s *MonthlyAnalyzer) save() error {
	index := mc.GetSummaryIndexName(s.startTime)
	client := elasticseach.NewClient(s.Logger)
	if err := client.CreateIndex(index); err != nil {
		s.Logger.Errorf("Create index %s failed, error %s", index, err.Error())
		return err
	}

	insert := func(records []*model.ResourceUsageSummaryRecord) {
		if len(records) < 1 {
			s.Logger.Infof("Has no summary records for this kind, skip")
			return
		}
		documents := make([]elasticseach.Document, len(records))
		for i, v := range records {
			mc.SetIDForMonthlySummaryRecord(v)
			documents[i] = v
			if common.LogLevel > 7 {
				result, _ := json.MarshalIndent(v, "", "   ")
				if v.Cluster == "" {
					s.Logger.Infof("Project %s, %s\n%s", v.Type, v.Project, string(result))
				} else {
					s.Logger.Infof("Namespace %s, %s/%s/%s\n%s", v.Type, v.Project, v.Cluster, v.Namespace, string(result))
				}
			}
		}
		numTotal, numErrors := client.InsertDocuments(index, documents)
		s.Logger.Infof("Insert %d summary records to ElasticSearch, failed %d", numTotal, numErrors)
	}
	insert(s.namespacePodUsageSummary)
	insert(s.namespaceQuotaSummary)
	insert(s.projectPodUsageSummary)
	insert(s.projectNamespaceQuotaSummary)
	insert(s.projectQuotaSummary)
	return nil
}

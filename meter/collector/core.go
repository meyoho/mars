package collector

import (
	"time"

	"mars/common"
	"mars/meter/collector/namespaces"
	"mars/meter/collector/pods"
	"mars/meter/collector/projects"
	"mars/meter/collector/storage"
	"mars/meter/collector/types"
	"mars/meter/fetcher"
)

type Controller struct {
	Fetcher    *fetcher.Fetcher
	Collectors []types.MeterCollector
	StartTime  time.Time
	EndTime    time.Time
	Logger     *common.Logger
}

func NewCollectorController(start time.Time, end time.Time) *Controller {
	logger := common.NewMeterLogger(map[string]string{"r": "core"})
	return &Controller{
		Fetcher:    fetcher.NewFetcher(),
		Collectors: []types.MeterCollector{},
		StartTime:  start,
		EndTime:    end,
		Logger:     logger,
	}
}

func (c *Controller) Run() {
	// Create project quota meter collector
	if len(c.Fetcher.Projects) > 0 {
		// Create project meter collector
		manager := storage.NewManager()
		collector := projects.NewCollector(c.Fetcher.Projects, manager, c.StartTime, c.EndTime)
		c.Collectors = append(c.Collectors, collector)
		c.Logger.Infof("Add projects collector for start time %s, end time %s", c.StartTime.Format(time.RFC3339), c.EndTime.Format(time.RFC3339))
		go collector.Collect()
	}

	// Create cluster meter collector
	for _, cluster := range c.Fetcher.Clusters {
		ns, ok := c.Fetcher.Namespaces[cluster.Name]
		if !ok {
			c.Logger.Infof("Has no namespaces in cluster %s? Continue", cluster.Name)
			continue
		}
		// Create namespace quota meter collector
		manager := storage.NewManager()
		namespaceCollector := namespaces.NewCollector(cluster, ns, c.Fetcher.ResourceQuotas[cluster.Name], manager, c.StartTime, c.EndTime)
		c.Collectors = append(c.Collectors, namespaceCollector)
		c.Logger.Infof("Add namespace quota collector for cluster %s, namespaces %d, start time %s, end time %s", cluster.Name, len(ns), c.StartTime.Format(time.RFC3339), c.EndTime.Format(time.RFC3339))
		go namespaceCollector.Collect()

		// Create pod meter collector
		manager = storage.NewManager()
		podCollector := pods.NewCollector(cluster, ns, manager, c.StartTime, c.EndTime)
		c.Collectors = append(c.Collectors, podCollector)
		c.Logger.Infof("Add pod collector for cluster %s, namespaces %d, start time %s, end time %s", cluster.Name, len(ns), c.StartTime.Format(time.RFC3339), c.EndTime.Format(time.RFC3339))
		go podCollector.Collect()
	}

	// Wait for all collector done
	for {
		done := true
		for _, collector := range c.Collectors {
			status := collector.GetStatus()
			if !status.Done {
				done = false
			}
			if status.Error == nil {
				c.Logger.Infof("Check collector %s: done %v", status.Name, status.Done)
			} else {
				c.Logger.Infof("Check collector %s: done %v, error %s", status.Name, status.Done, status.Error.Error())
			}
		}
		if done {
			c.Logger.Infof("All collectors had done, break")
			break
		} else {
			time.Sleep(time.Minute * 1)
		}
	}
}

package namespaces

import (
	"time"

	"mars/common"
	"mars/meter/collector/types"
	mc "mars/meter/common"
	"mars/model"
)

type Collector struct {
	cluster        *model.Cluster
	namespaces     []*model.NamespaceResource
	resourceQuotas []*model.ResourceQuotaResource
	startTime      time.Time
	endTime        time.Time
	status         *types.CollectorStatus
	storageManager types.MeterStorageManager
	logger         *common.Logger
}

func NewCollector(c *model.Cluster, ns []*model.NamespaceResource, rq []*model.ResourceQuotaResource, m types.MeterStorageManager, start time.Time, end time.Time) *Collector {
	if rq == nil {
		rq = []*model.ResourceQuotaResource{}
	}
	return &Collector{
		cluster:        c,
		namespaces:     ns,
		resourceQuotas: rq,
		startTime:      start,
		endTime:        end,
		storageManager: m,
		status:         &types.CollectorStatus{Name: c.Name + "-namespace-collector", Done: false, Error: nil},
		logger:         common.NewMeterLogger(map[string]string{"r": "project_collector"}),
	}
}

func (c *Collector) GetStatus() *types.CollectorStatus {
	return c.status
}

func (c *Collector) Collect() {
	records := map[string]*model.ResourceUsageRecord{}

	for _, n := range c.namespaces {
		record := &model.ResourceUsageRecord{
			Project:   n.GetProject(),
			Cluster:   c.cluster.Name,
			Namespace: n.Name,
			Type:      mc.NamespaceQuota,
			Meter: &model.Meter{
				CPURequests:    0,
				MemoryRequests: 0,
			},
			Day:       c.startTime.Format("20060102"),
			StartTime: c.startTime,
			EndTime:   c.endTime,
		}
		for _, q := range c.resourceQuotas {
			if q.Namespace == n.Name && q.Name == "default" {
				record.Meter.CPURequests = c.parseCPURequests(q) * c.endTime.Sub(c.startTime).Hours()
				record.Meter.MemoryRequests = c.parseMemoryRequests(q) * c.endTime.Sub(c.startTime).Hours()
			}
		}
		if record.EndTime.Hour() == 0 && record.EndTime.Minute() == 0 && record.EndTime.Second() == 0 {
			record.EndTime = record.EndTime.Add(-1)
		}
		records[c.cluster.Name+"___"+n.Name] = record
	}

	if err := c.storageManager.Insert(records); err != nil {
		c.status.Error = err
	}
	c.status.Done = true
}

func (c *Collector) parseCPURequests(q *model.ResourceQuotaResource) float64 {
	for k, v := range q.Spec.Hard {
		if k == "requests.cpu" {
			return float64(v.MilliValue()) / 1000
		}
	}
	return 0
}

func (c *Collector) parseMemoryRequests(q *model.ResourceQuotaResource) float64 {
	for k, v := range q.Spec.Hard {
		if k == "requests.memory" {
			return float64(v.Value()) / 1024 / 1024 / 1024
		}
	}
	return 0
}

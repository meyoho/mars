package pods

import (
	"fmt"
	"time"

	"mars/common"
	"mars/infra/prometheus"
	"mars/meter/collector/types"
	mc "mars/meter/common"
	"mars/model"

	pm "github.com/prometheus/common/model"
)

type Collector struct {
	cluster        *model.Cluster
	namespaces     []*model.NamespaceResource
	startTime      time.Time
	endTime        time.Time
	status         *types.CollectorStatus
	storageManager types.MeterStorageManager
	logger         *common.Logger
}

func NewCollector(c *model.Cluster, ns []*model.NamespaceResource, m types.MeterStorageManager, start time.Time, end time.Time) *Collector {
	return &Collector{
		cluster:        c,
		namespaces:     ns,
		startTime:      start,
		endTime:        end,
		storageManager: m,
		status:         &types.CollectorStatus{Name: c.Name+"-pod-collector", Done: false, Error: nil},
		logger:         common.NewMeterLogger(map[string]string{"r": "pod_collector", "c": c.Name}),
	}
}

func (c *Collector) GetStatus() *types.CollectorStatus {
	return c.status
}

func (c *Collector) Collect() {
	if !c.cluster.HasMetricFeature {
		c.status.Error = fmt.Errorf("cluster %s has no prometheus installed, skip", c.cluster.Name)
		c.status.Done = true
		return
	}
	records, err := c.GetRecords()
	if err != nil {
		c.status.Error = err
	}
	if err := c.storageManager.Insert(records); err != nil {
		c.status.Error = err
	}
	c.status.Done = true
}

func (c *Collector) GetRecords() (map[string]*model.ResourceUsageRecord, error) {
	records := map[string]*model.ResourceUsageRecord{}

	getRecord := func(namespace string, pod string) *model.ResourceUsageRecord {
		project := ""
		for _, ns := range c.namespaces {
			if ns.Name == namespace {
				project = ns.GetProject()
			}
		}
		record := &model.ResourceUsageRecord{}
		find := false
		for _, r := range records {
			if r.Namespace == namespace && r.Pod == pod {
				find = true
				record = r
				break
			}
		}
		if !find {
			record = &model.ResourceUsageRecord{
				Project:   project,
				Cluster:   c.cluster.Name,
				Namespace: namespace,
				Pod:       pod,
				Type:      mc.PodUsage,
				Meter:     &model.Meter{},
				Day:       c.startTime.Format("20060102"),
				StartTime: c.startTime,
				EndTime:   c.startTime,
			}
			records[namespace+"___"+pod] = record
		}
		return record
	}

	updateRecord := func(record *model.ResourceUsageRecord, stream *pm.SampleStream, t string) *model.ResourceUsageRecord {
		record.Day = c.startTime.Format("20060102")
		record.StartTime = stream.Values[0].Timestamp.Time()
		record.EndTime = stream.Values[len(stream.Values)-1].Timestamp.Time()
		if record.EndTime.Hour() == 0 && record.EndTime.Minute() ==0 && record.EndTime.Second() == 0 {
			record.EndTime = record.EndTime.Add(-1)
		}
		var usage float64
		for _, v := range stream.Values[1:] {
			usage += float64(v.Value)
		}
		switch t {
		case PodCPUUsage:
			record.Meter.CPUUsage = usage / (3600 / types.Step)
		case PodMemoryUsage:
			record.Meter.MemoryUsage = usage / (3600 / types.Step)
		case PodCPURequests:
			record.Meter.CPURequests = usage / (3600 / types.Step)
		case PodMemoryRequests:
			record.Meter.MemoryRequests = usage / (3600 / types.Step)
		}
		return record
	}

	collect := func(indicator string) error {
		query, ok := QuerySet[indicator]
		if !ok {
			return fmt.Errorf("cam not find query in query set for indicator " + indicator)
		}
		client := prometheus.NewClient(*c.cluster.PrometheusConfig, c.logger)
		response, err := client.GetRange(query, c.startTime.Unix(), c.endTime.Unix(), types.Step)
		if err != nil {
			return err
		}
		for _, v := range response.(pm.Matrix) {
			pod := string(v.Metric["pod_name"])
			if pod == "" {
				pod = string(v.Metric["pod"])
			}
			namespace := string(v.Metric["namespace"])
			if len(v.Values) < 2 {
				continue
			}
			record := getRecord(namespace, pod)
			record = updateRecord(record, v, indicator)
		}
		return nil
	}
	for _, indicator := range IndicatorList {
		if err := collect(indicator); err != nil {
			return records, err
		}
	}
	return records, nil
}

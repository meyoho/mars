package pods

var (
	PodCPUUsage       = "PodCPUUsage"
	PodMemoryUsage    = "PodMemoryUsage"
	PodCPURequests    = "PodCPURequests"
	PodMemoryRequests = "PodMemoryRequests"

	IndicatorList = []string{PodCPUUsage, PodMemoryUsage, PodCPURequests, PodMemoryRequests}

	QuerySet = map[string]string{
		PodCPUUsage:       `sum (container_cpu_usage_seconds_total_irate5m{pod_name!="",container_name!="",container_name!="POD"}) by (namespace, pod_name)`,
		PodMemoryUsage:    `sum (container_memory_usage_bytes_without_cache) by (namespace, pod_name) / 1073741824`,
		PodCPURequests:    `sum (kube_pod_container_resource_requests_cpu_cores) by (namespace, pod)`,
		PodMemoryRequests: `sum (kube_pod_container_resource_requests_memory_bytes) by (namespace, pod) / 1073741824`,
	}
)

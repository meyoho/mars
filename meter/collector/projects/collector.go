package projects

import (
	"time"

	"mars/common"
	"mars/meter/collector/types"
	mc "mars/meter/common"
	"mars/model"

	"k8s.io/apimachinery/pkg/api/resource"
)

type Collector struct {
	projects       []*model.ProjectResource
	startTime      time.Time
	endTime        time.Time
	status         *types.CollectorStatus
	storageManager types.MeterStorageManager
	logger         *common.Logger
}

func NewCollector(ps []*model.ProjectResource, m types.MeterStorageManager, start time.Time, end time.Time) *Collector {
	return &Collector{
		projects:       ps,
		startTime:      start,
		endTime:        end,
		storageManager: m,
		status:         &types.CollectorStatus{Name: "project-collector", Done: false, Error: nil},
		logger:         common.NewMeterLogger(map[string]string{"r": "project_collector"}),
	}
}

func (c *Collector) GetStatus() *types.CollectorStatus {
	return c.status
}

func (c *Collector) Collect() {
	records := map[string]*model.ResourceUsageRecord{}

	for _, p := range c.projects {
		cpuRequests, err := c.parseCPURequests(p)
		if err != nil {
			c.status.Error = err
			continue
		}
		memoryRequests, err := c.parseMemoryRequests(p)
		if err != nil {
			c.status.Error = err
			continue
		}
		record := &model.ResourceUsageRecord{
			Project: p.Name,
			Type:    mc.ProjectQuota,
			Meter: &model.Meter{
				CPURequests:    cpuRequests * c.endTime.Sub(c.startTime).Hours(),
				MemoryRequests: memoryRequests * c.endTime.Sub(c.startTime).Hours(),
			},
			Day:       c.startTime.Format("20060102"),
			StartTime: c.startTime,
			EndTime:   c.endTime,
		}
		if record.EndTime.Hour() == 0 && record.EndTime.Minute() ==0 && record.EndTime.Second() == 0 {
			record.EndTime = record.EndTime.Add(-1)
		}
		records[p.Name] = record
	}

	if err := c.storageManager.Insert(records); err != nil {
		c.status.Error = err
	}
	c.status.Done = true
}

func (c *Collector) parseCPURequests(project *model.ProjectResource) (float64, error) {
	var cpus float64
	for _, c := range project.Spec.Clusters {
		if c.Quota.RequestsCPU == "" {
			continue
		}
		q, err := resource.ParseQuantity(c.Quota.RequestsCPU)
		if err != nil {
			return 0, err
		}
		cpus += float64(q.MilliValue()) / 1000
	}
	return cpus, nil
}

func (c *Collector) parseMemoryRequests(project *model.ProjectResource) (float64, error) {
	var memory float64
	for _, c := range project.Spec.Clusters {
		if c.Quota.RequestsMemory == "" {
			continue
		}
		q, err := resource.ParseQuantity(c.Quota.RequestsMemory)
		if err != nil {
			return 0, err
		}
		memory += float64(q.Value()) / 1024 / 1024 / 1024
	}
	return memory, nil
}

package storage

import (
	"encoding/json"

	"mars/common"
	"mars/infra/elasticseach"
	mc "mars/meter/common"
	"mars/model"
)

type Manager struct {
	logger *common.Logger
}

func NewManager() *Manager {
	return &Manager{logger: common.NewMeterLogger(map[string]string{"r": "storage"})}
}

func (m *Manager) Insert(records map[string]*model.ResourceUsageRecord) error {
	if len(records) < 1 {
		return nil
	}
	i := 0
	documents := make([]elasticseach.Document, len(records))
	for k, v := range records {
		mc.SetIDForRecord(v)
		documents[i] = v
		i++
		if common.LogLevel > 7 {
			result, err := json.MarshalIndent(v, "", "   ")
			if err != nil {
				return err
			}
			m.logger.Infof("%s\n%s", k, string(result))
		}
	}
	index := ""
	for _, v := range records {
		index = mc.GetDetailIndexName(v.StartTime)
		break
	}
	client := elasticseach.NewClient(m.logger)
	if err := client.CreateIndex(index); err != nil {
		m.logger.Errorf("Create index %s failed, error %s", index, err.Error())
		return err
	}
	numTotal, numErrors := client.InsertDocuments(index, documents)
	m.logger.Infof("Insert %d records to ElasticSearch, failed %d", numTotal, numErrors)
	return nil
}

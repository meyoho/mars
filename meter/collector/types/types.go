package types

import (
	"mars/model"
)

const (
	Step = 300
)

type CollectorStatus struct {
	Name  string
	Done  bool
	Error error
}

type MeterCollector interface {
	Collect()
	GetStatus() *CollectorStatus
}

type MeterStorageManager interface {
	Insert(records map[string]*model.ResourceUsageRecord) error
}

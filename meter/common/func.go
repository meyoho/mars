package common

import (
	"mars/config"

	wg "alauda.io/warpgate"
)

const MeterFeatureGate = "meter"

func MeterEnabled() (bool, error) {
	featureGate := wg.NewWarpGate(wg.Config{
		APIEndpoint:        config.GlobalConfig.Archon.Endpoint,
		AuthorizationToken: config.GlobalConfig.Kubernetes.Token,
	})
	return featureGate.IsFeatureGateEnabled(MeterFeatureGate)
}

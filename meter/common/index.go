package common

import (
	"strings"
	"time"

	"mars/common"
	"mars/model"
)

func GetDetailIndexName(t time.Time) string {
	return "meter-detail-" + t.Format("200601")
}

func GetSummaryIndexName(t time.Time) string {
	return "meter-summary-" + t.Format("200601")
}

func SetIDForRecord(record *model.ResourceUsageRecord) {
	uniqueSlice := []string{
		record.Project,
		record.Cluster,
		record.Namespace,
		record.Pod,
		record.Type,
		record.StartTime.Format("20060102"),
	}
	record.ID = common.MD5(strings.Join(uniqueSlice, "___"))
}

func SetIDForDailySummaryRecord(record *model.ResourceUsageSummaryRecord) {
	uniqueSlice := []string{
		record.Project,
		record.Cluster,
		record.Namespace,
		record.Pod,
		record.Type,
		record.StartTime.Format("20060102"),
	}
	record.ID = common.MD5(strings.Join(uniqueSlice, "___"))
}

func SetIDForMonthlySummaryRecord(record *model.ResourceUsageSummaryRecord) {
	uniqueSlice := []string{
		record.Project,
		record.Cluster,
		record.Namespace,
		record.Pod,
		record.Type,
		record.StartTime.Format("200601"),
	}
	record.ID = common.MD5(strings.Join(uniqueSlice, "___"))
}

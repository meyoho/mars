package common

const (
	ProjectQuota   = "projectQuota"
	NamespaceQuota = "namespaceQuota"
	PodUsage       = "podUsage"

	PeriodDaily   = "daily"
	PeriodMonthly = "monthly"
)

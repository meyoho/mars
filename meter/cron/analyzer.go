package cron

import (
	"time"

	"mars/meter/analyzer"
	mc "mars/meter/common"
)

type DailyAnalyzer struct{}

func (c *DailyAnalyzer) Run() {
	// if meter feature gate not enabled, skip job
	if enabled, err := mc.MeterEnabled(); !enabled {
		if err != nil {
			Logger.Errorf("Get meter gate error, %s, skip", err.Error())
		} else {
			Logger.Errorf("Meter gate has been closed, skip")
		}
		return
	}

	t := time.Now().UTC()
	year, month, day := t.Date()
	t1 := time.Date(year, month, day, 0, 0, 0, 0, t.Location())
	start := t1.Add(-time.Hour * 24)
	end := t1
	controller := analyzer.NewDailyAnalyzer(start, end)
	controller.Run()
}

type MonthlyAnalyzer struct{}

func (c *MonthlyAnalyzer) Run() {
	// if meter feature gate not enabled, skip job
	if enabled, _ := mc.MeterEnabled(); !enabled {
		return
	}

	t := time.Now().UTC()
	year, month, day := t.Date()
	start := time.Date(year, month, 1, 0, 0, 0, 0, t.Location())
	end := time.Date(year, month, day, 0, 0, 0, 0, t.Location())
	controller := analyzer.NewMonthlyAnalyzer(start, end)
	controller.Run()
}

package cron

import (
	"time"

	"mars/config"
	"mars/meter/collector"
	mc "mars/meter/common"
)

type Collector struct{}

func (c *Collector) Run() {
	// if meter feature gate not enabled, skip job
	if enabled, err := mc.MeterEnabled(); !enabled {
		if err != nil {
			Logger.Errorf("Get meter gate error, %s, skip", err.Error())
		} else {
			Logger.Errorf("Meter gate has been closed, skip")
		}
		return
	}

	t := time.Now().UTC()
	year, month, day := t.Date()
	t1 := time.Date(year, month, day, 0, 0, 0, 0, t.Location())
	t2 := time.Date(year, month, day, t.Hour(), 0, 0, 0, t.Location())
	var start, end time.Time
	if t.Hour() >= config.GlobalConfig.Meter.CollectorIntervalInHours {
		start = t1
		end = t2
	} else {
		start = t1.Add(-time.Hour * 24)
		end = t2.Add(-time.Hour * time.Duration(t.Hour()))
	}
	controller := collector.NewCollectorController(start, end)
	controller.Run()
}

package cron

import (
	"mars/common"
	"mars/config"

	"github.com/robfig/cron"
)

var Logger = common.NewMeterLogger(map[string]string{"r": "cron"})

func Start() {
	c := cron.New()

	// Add collector job
	collectorCronJobSpec := "0 5 */2 * * ?"
	if config.GlobalConfig.Meter.CollectorCronJobSpec != "" && config.GlobalConfig.Meter.CollectorCronJobSpec != "default" {
		collectorCronJobSpec = config.GlobalConfig.Meter.CollectorCronJobSpec
	}
	if err := c.AddJob(collectorCronJobSpec, &Collector{}); err != nil {
		panic(err.Error())
	}

	// Add daily analyzer job
	analyzerDailyCronJobSpec := "0 10 */4 * * ?"
	if config.GlobalConfig.Meter.AnalyzerDailyCronJobSpec != "" && config.GlobalConfig.Meter.AnalyzerDailyCronJobSpec != "default" {
		analyzerDailyCronJobSpec = config.GlobalConfig.Meter.AnalyzerDailyCronJobSpec
	}
	if err := c.AddJob(analyzerDailyCronJobSpec, &DailyAnalyzer{}); err != nil {
		panic(err.Error())
	}

	// Add monthly analyzer job
	analyzerMonthlyCronJobSpec := "0 15 */6 * * ?"
	if config.GlobalConfig.Meter.AnalyzerMonthlyCronJobSpec != "" && config.GlobalConfig.Meter.AnalyzerMonthlyCronJobSpec != "default" {
		analyzerMonthlyCronJobSpec = config.GlobalConfig.Meter.AnalyzerMonthlyCronJobSpec
	}
	if err := c.AddJob(analyzerMonthlyCronJobSpec, &MonthlyAnalyzer{}); err != nil {
		panic(err.Error())
	}

	// Start jobs
	c.Start()
	defer c.Stop()
	select {}
}

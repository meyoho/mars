package fetcher

import (
	"time"

	"mars/infra/kubernetes"

	"k8s.io/client-go/rest"
)

const (
	MaxRetryCounts = 3
)

func Retry(client *kubernetes.Client, method string, resource kubernetes.Resource, params map[string]string) (*rest.Result, error) {
	var retryCount = 0
	var err error
	var result *rest.Result
	for {
		if retryCount > MaxRetryCounts {
			return nil, err
		}
		retryCount++
		result, err = client.Request(method, resource, params)
		if err != nil {
			time.Sleep(time.Second * 10)
			continue
		}
		return result, nil
	}
}

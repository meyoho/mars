package fetcher

import (
	"fmt"
	"time"

	"mars/cache/cluster"
	"mars/common"
	"mars/model"
)

type Fetcher struct {
	Clusters       []*model.Cluster
	Projects       []*model.ProjectResource
	Namespaces     map[string][]*model.NamespaceResource
	ResourceQuotas map[string][]*model.ResourceQuotaResource
	Ready          bool
	Error          error
	Logger         *common.Logger
}

func NewFetcher() *Fetcher {
	f := &Fetcher{
		Clusters:       nil,
		Projects:       nil,
		Namespaces:     nil,
		ResourceQuotas: nil,
		Ready:          false,
		Logger:         common.NewMeterLogger(map[string]string{"r": "meter_fetcher"}),
	}
	f.Run()
	return f
}

func (f *Fetcher) Run() {
	for {
		if cluster.IsCacheReady() {
			break
		}
		f.Logger.Infof("Cluster cache is not ready, wait...")
		time.Sleep(time.Second * 10)
	}
	f.Clusters = cluster.ListClusters()

	if err := f.fetchProjectsResource(); err != nil {
		f.Error = err
		f.Ready = false
		f.Logger.Errorf("Fetch projects resource error, %s", err.Error())
	}
	if err := f.fetchNamespaceResource(); err != nil {
		f.Error = err
		f.Ready = false
		f.Logger.Errorf("Fetch namespace resource error, %s", err.Error())
	}
	if err := f.fetchResourceQuotaResource(); err != nil {
		f.Error = err
		f.Ready = false
		f.Logger.Errorf("Fetch namespace resource quota error, %s", err.Error())
	}
	f.Ready = true
}

func (f *Fetcher) GetProjects() ([]*model.ProjectResource, error) {
	if !f.Ready {
		return nil, fmt.Errorf("fetcher is not ready, please wait")
	}
	return f.Projects, nil
}

func (f *Fetcher) GetNamespaces(c *model.Cluster) ([]*model.NamespaceResource, error) {
	if !f.Ready {
		return nil, fmt.Errorf("fetcher is not ready, please wait")
	}
	nrs, ok := f.Namespaces[c.Name]
	if !ok {
		return []*model.NamespaceResource{}, nil
	}
	return nrs, nil
}

func (f *Fetcher) GetNamespaceResourceQuota(c *model.Cluster, n *model.NamespaceResource) (*model.ResourceQuotaResource, error) {
	if !f.Ready {
		return nil, fmt.Errorf("fetcher is not ready, please wait")
	}
	quotas, ok := f.ResourceQuotas[c.Name]
	if !ok {
		return nil, nil
	}
	for _, q := range quotas {
		if q.Name == "default" && q.Namespace == n.Name {
			return q, nil
		}
	}
	return nil, nil
}

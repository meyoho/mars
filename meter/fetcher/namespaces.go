package fetcher

import (
	"encoding/json"
	"k8s.io/client-go/rest"

	"mars/common"
	"mars/infra/kubernetes"
	"mars/model"

	"github.com/juju/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (f *Fetcher) fetchNamespaceResource() error {
	if f.Namespaces == nil {
		f.Namespaces = make(map[string][]*model.NamespaceResource, 0)
	}
	for _, c := range f.Clusters {
		f.Logger.Infof("Try to fetch namespace resources for cluster %s", c.Name)
		resource := &model.ResourceMeta{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: model.AVKubernetesV1,
				Kind:       model.RKNamespace,
			},
		}
		client := kubernetes.NewClient(c, resource, f.Logger)
		result, err := Retry(client, common.HttpGet, resource, nil)
		if err != nil {
			f.Logger.Errorf("Get namespace resources error, continue, %s", err.Error())
			continue
		}
		resources, err := f.restToNamespaceArray(result)
		if err != nil {
			f.Logger.Errorf("Rest result to namespace resources error, continue, %s", err.Error())
			continue
		}
		f.Namespaces[c.Name] = resources
		f.Logger.Infof("Fetched %d namespaces resources for %s", len(resources), c.Name)
	}
	return nil
}

func (f *Fetcher) restToNamespaceArray(result *rest.Result) ([]*model.NamespaceResource, error) {
	body, err := result.Raw()
	if err != nil {
		return nil, errors.Annotate(err, "get raw bytes error for rest client result")
	}

	var object model.NamespaceList
	if err := json.Unmarshal(body, &object); err != nil {
		return nil, err
	}
	return object.Items, nil
}

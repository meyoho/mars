package fetcher

import (
	"encoding/json"
	"fmt"
	"k8s.io/client-go/rest"

	"mars/common"
	"mars/infra/kubernetes"
	"mars/model"

	"github.com/juju/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (f *Fetcher) fetchProjectsResource() error {
	f.Logger.Infof("Try to fetch project resources")
	var global *model.Cluster
	for _, c := range f.Clusters {
		if c.IsGlobal {
			global = c
			break
		}
	}
	if global == nil {
		return fmt.Errorf("can not find global cluster")
	}

	resource := &model.ResourceMeta{
		TypeMeta: metaV1.TypeMeta{
			APIVersion: model.AVProjectV1,
			Kind:       model.RKProject,
		},
	}
	client := kubernetes.NewClient(global, resource, f.Logger)
	result, err := Retry(client, common.HttpGet, resource, nil)
	if err != nil {
		return errors.Annotate(err, "get project resources error")
	}
	resources, err := f.restToProjectArray(result)
	if err != nil {
		return errors.Annotate(err, "rest to resource list error")
	}
	f.Projects = resources
	f.Logger.Infof("Fetched %d project resources", len(f.Projects))
	return nil
}

func (f *Fetcher) restToProjectArray(result *rest.Result) ([]*model.ProjectResource, error) {
	body, err := result.Raw()
	if err != nil {
		return nil, errors.Annotate(err, "get raw bytes error for rest client result")
	}

	var object model.ProjectList
	if err := json.Unmarshal(body, &object); err != nil {
		return nil, err
	}
	return object.Items, nil
}

package fetcher

import (
	"encoding/json"
	"k8s.io/client-go/rest"

	"mars/common"
	"mars/infra/kubernetes"
	"mars/model"

	"github.com/juju/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (f *Fetcher) fetchResourceQuotaResource() error {
	if f.ResourceQuotas == nil {
		f.ResourceQuotas = make(map[string][]*model.ResourceQuotaResource, 0)
	}
	for _, c := range f.Clusters {
		resource := &model.ResourceMeta{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: model.AVKubernetesV1,
				Kind:       model.RKResourceQuota,
			},
		}
		f.Logger.Infof("Try to fetch resource quotas for cluster %s", c.Name)
		client := kubernetes.NewClient(c, resource, f.Logger)
		result, err := Retry(client, common.HttpGet, resource, nil)
		if err != nil {
			f.Logger.Errorf("Get resource quota error, continue, %s", err.Error())
			continue
		}
		resources, err := f.restToResourceQuotaArray(result)
		if err != nil {
			f.Logger.Errorf("Rest result to resource quota error, continue, %s", err.Error())
			continue
		}
		f.ResourceQuotas[c.Name] = resources
		f.Logger.Infof("Fetched %d resource quotas for %s", len(resources), c.Name)
	}
	return nil
}

func (f *Fetcher) restToResourceQuotaArray(result *rest.Result) ([]*model.ResourceQuotaResource, error) {
	body, err := result.Raw()
	if err != nil {
		return nil, errors.Annotate(err, "get raw bytes error for rest client result")
	}

	var object model.ResourceQuotaList
	if err := json.Unmarshal(body, &object); err != nil {
		return nil, err
	}
	return object.Items, nil
}

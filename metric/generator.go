package metric

import (
	"encoding/json"
	"strings"
	"sync"
	"time"

	"mars/cache/alert"
	"mars/common"
	"mars/config"
	"mars/model"
)

type Generator struct {
	cluster   *model.Cluster
	alerts    []*model.AlertResponse
	stop      bool
	createdAt time.Time
	updatedAt time.Time
	Logger    *common.Logger
}

func NewGenerator(c *model.Cluster) *Generator {
	generator := &Generator{
		cluster:   c,
		createdAt: time.Now().UTC(),
		updatedAt: time.Now().UTC(),
		Logger:    common.NewLogger(map[string]string{"c": c.Name, "r": "mg"}),
	}
	go generator.Run()
	return generator
}

func (g *Generator) Run() {
	for {
		if g.stop {
			g.Logger.Infof("It's time to exit as a stop signal received")
			break
		}

		alerts := g.getAlertList()
		for i, a := range alerts {
			g.Logger.Debugf("Find alert [%d]: %s/%s/%s", i, a.ResourceName, a.GroupName, a.Name)
		}

		t := time.Now().UTC().Add(-time.Second * time.Duration(config.GlobalConfig.Mars.MetricLatency))
		if !t.After(g.updatedAt) {
			g.Logger.Infof("No, it's not time to execute elasticsearch query, continue")
			time.Sleep(time.Second * 2)
			continue
		}

		err := parallel(alerts, func(alert *model.AlertResponse) error {
			g.AddLogMetrics(alert, t)
			g.AddEventMetrics(alert, t)
			return nil
		})
		if err != nil {
			g.Logger.Errorf("No, an error occurs in parallel %s", err.Error())
		}

		g.updatedAt = t
		sleepSeconds := t.Unix() + config.GlobalConfig.Mars.MetricLatency + config.GlobalConfig.Mars.MetricInterval - time.Now().UTC().Unix()
		if sleepSeconds > 0 {
			g.Logger.Debugf("I will sleep %v seconds", sleepSeconds)
			time.Sleep(time.Second * time.Duration(sleepSeconds))
		} else {
			g.Logger.Infof("I can not sleep(%v), I must hurry up!", sleepSeconds)
		}
	}
}

func (g *Generator) Stop() bool {
	g.stop = true
	return g.stop
}

func parallel(alerts []*model.AlertResponse, fn func(alert *model.AlertResponse) error) error {
	var wg sync.WaitGroup
	errCh := make(chan error, 1)
	for _, a := range alerts {
		wg.Add(1)
		// Concurrently calls prometheus query range api to reduce api latency
		go func(alert *model.AlertResponse) {
			defer wg.Done()
			if err := fn(alert); err != nil {
				select {
				case errCh <- err:
				default:
				}
			}
		}(a)
	}
	wg.Wait()

	// All queries will be executed at the same time, the first error will be returned if any
	select {
	case err := <-errCh:
		return err
	default:
		return nil
	}
}

func (g *Generator) getAlertList() []*model.AlertResponse {
	filter := func(alerts []*model.AlertResponse) []*model.AlertResponse {
		result := make([]*model.AlertResponse, 0)
		uniqueNameSet := make(map[string]bool, 0)
		for _, item := range alerts {
			m := item.Metric.Queries[0].GetMeta()
			uniqueName := strings.Join([]string{m.Namespace, m.Kind, m.Name, m.Query}, "___")
			if _, ok := uniqueNameSet[uniqueName]; ok {
				continue
			}
			uniqueNameSet[uniqueName] = true
			result = append(result, item)
		}
		return result
	}

	result := make([]*model.AlertResponse, 0)
	resources := alert.GetAlertResourceForCluster(g.cluster)
	for _, resource := range resources {
		slice := g.alertResourceToAlertResponses(resource)
		result = append(result, slice...)
	}
	return filter(result)
}

// AlertResourceToAlertResponses will translate alert resource to alert responses. Please note that,
// if a rule has no mirror in annotations, this function will generate an alert response with only
// some basic fields. If you want to use some other fields, you must enhance this function manually.
func (g *Generator) alertResourceToAlertResponses(resource *model.AlertResource) []*model.AlertResponse {
	result := make([]*model.AlertResponse, 0)
	owner := strings.ToLower(resource.Labels[model.LKOwner])
	if !(owner == "system" || owner == "alaudasystem") {
		return result
	}
	for _, group := range resource.Spec.Groups {
		for _, rule := range group.Rules {
			item := model.AlertResponse{}
			// If a rule has a mirror in annotations, just decoded it
			if meta, ok := rule.Annotations[model.AlertMetaAnnotationKey]; ok {
				if err := json.Unmarshal([]byte(meta), &item); err != nil {
					g.Logger.Errorf("Unmarshal alert meta error %s, skip", err.Error())
					continue
				}
				item.GroupName = group.Name
				item.ResourceName = resource.Name
				result = append(result, &item)
				continue
			}
			// If a rule has no mirror in annotations, generate an alert response with some basic fields
			item.Name = rule.Alert
			item.ResourceName = resource.Name
			item.GroupName = group.Name
			if rule.Labels["alert_indicator_log_query"] != "" && rule.Labels["alert_indicator_query"] == "" {
				rule.Labels["alert_indicator_query"] = rule.Labels["alert_indicator_log_query"]
			}
			item.Metric = model.QueryRangeRequest{
				Queries: []model.QueryExpression{
					{
						Labels: []model.LabelExpression{
							{
								Name:  "__name__",
								Value: rule.Labels["alert_indicator"],
							},
							{
								Name:  "kind",
								Value: rule.Labels["alert_involved_object_kind"],
							},
							{
								Name:  "name",
								Value: rule.Labels["alert_involved_object_name"],
							},
							{
								Name:  "namespace",
								Value: rule.Labels["alert_involved_object_namespace"],
							},
							{
								Name:  "application",
								Value: rule.Labels["application"],
							},
							{
								Name:  "query",
								Value: rule.Labels["alert_indicator_query"],
							},
						},
					},
				},
			}
			result = append(result, &item)
		}
	}
	return result
}

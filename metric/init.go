package metric

import (
	"github.com/prometheus/client_golang/prometheus"
)

// Counter metric for log count
var logCounter = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "workload_log_keyword_count",
		Help: "workload log keyword counter",
	},
	[]string{"cluster_name", "cluster_uuid", "namespace", "application", "kind", "name", "query"},
)

// Counter metric for event count
var eventCounter = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "workload_event_reason_count",
		Help: "workload event reason counter",
	},
	[]string{"cluster_name", "cluster_uuid", "namespace", "application", "kind", "name", "query"},
)

// Register metrics before server start
func init() {
	prometheus.MustRegister(logCounter)
	prometheus.MustRegister(eventCounter)
}

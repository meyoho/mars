package metric

import (
	"strings"
	"time"

	"mars/common"
	"mars/infra/elasticseach"
	"mars/model"
)

func (g *Generator) AddLogMetrics(alert *model.AlertResponse, end time.Time) error {
	fields := g.Logger.GetFields()
	fields["a"] = strings.Join([]string{alert.ResourceName, alert.GroupName, alert.Name}, "/")
	logger := common.NewLogger(fields)
	m := alert.Metric.Queries[0].GetMeta()
	if m.Indicator != model.WorkloadLogKeywordCount {
		return nil
	}
	meta := model.LogMeta{
		Indicator: m.Indicator,
		Cluster:   g.cluster.Name,
		Namespace: m.Namespace,
		PodName:   m.GetPodNamePattern(),
		Query:     m.Query,
		Start:     g.updatedAt.UTC(),
		End:       end.UTC(),
	}
	count := elasticseach.NewClient(g.Logger).GetLogCount(&meta)
	logger.Infof("Add %f for %s", count, meta.Indicator)
	values := []string{
		g.cluster.Name, // label: cluster_name
		g.cluster.UUID, // label: cluster_uuid
		m.Namespace,    // label: namespace
		m.Application,  // label: application
		m.Kind,         // label: kind
		m.Name,         // label: name
		m.Query,        // label: query
	}
	logCounter.WithLabelValues(values...).Add(count)
	return nil
}

package model

import (
	"time"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	// Alert api version and kind
	PrometheusRuleGroup   = "monitoring.coreos.com"
	PrometheusRuleVersion = "v1"
	PrometheusRuleKind    = "PrometheusRule"

	// Alert constants
	AlertMetaAnnotationKey = "AlertMeta"

	// Alert label names
	AlertIndicatorLabel   = "__name__"
	AlertNameLabel        = "name"
	AlertKindLabel        = "kind"
	AlertNamespaceLabel   = "namespace"
	AlertExprLabel        = "expr"
	AlertQueryLabel       = "query"
	AlertApplicationLabel = "application"
)

type AlertTemplate struct {
	Name          string            `json:"name"`
	Metric        QueryRangeRequest `json:"metric"`
	Compare       string            `json:"compare"`
	Threshold     float64           `json:"threshold"`
	Unit          string            `json:"unit,omitempty"`
	Wait          int64             `json:"wait"`
	Labels        map[string]string `json:"labels,omitempty"`
	Annotations   map[string]string `json:"annotations,omitempty"`
	Notifications []*Notification   `json:"notifications,omitempty"`
	ScaleUp       []*Services       `json:"scale_up,omitempty"`
	ScaleDown     []*Services       `json:"scale_down,omitempty"`
}

type AlertRequest struct {
	UUID             string            `json:"uuid"`
	ResourceName     string            `json:"resourceName"`
	GroupName        string            `json:"groupName"`
	Project          string            `json:"project"`
	Creator          string            `json:"creator"`
	CreatedAt        time.Time         `json:"created_at"`
	UpdatedAt        time.Time         `json:"updated_at"`
	AlertTemplate                      `json:",inline"`
	AdditionalConfig map[string]string `json:"additionalConfig,omitempty"`
}

type AlertResponse struct {
	AlertRequest        `json:",inline"`
	State        string `json:"state"`
	GroupName    string `json:"group_name"`
	ResourceName string `json:"resource_name"`
}

type Notification struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}

type Services struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`
}

type AlertResource struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`
	Spec AlertSpec    `json:"spec,omitempty"`
}

type AlertResourceList struct {
	metaV1.TypeMeta        `json:",inline"`
	metaV1.ObjectMeta      `json:"metadata,omitempty"`
	Items []*AlertResource `json:"items,omitempty"`
}

type AlertSpec struct {
	Groups []*AlertGroup `json:"groups"`
}

type AlertGroup struct {
	Name  string       `yaml:"name,omitempty" json:"name,omitempty"`
	Rules []*AlertRule `yaml:"rules" json:"rules"`
}

type AlertRule struct {
	Meta        *QueryMeta        `json:"-"`
	Alert       string            `yaml:"alert,omitempty" json:"alert,omitempty"`
	Expr        string            `yaml:"expr,omitempty" json:"expr,omitempty"`
	For         string            `yaml:"for,omitempty" json:"for,omitempty"`
	Labels      map[string]string `yaml:"labels,omitempty" json:"labels,omitempty"`
	Annotations map[string]string `yaml:"annotations,omitempty" json:"annotations,omitempty"`
}

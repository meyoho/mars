package model

import (
	"fmt"
	"mars/config"
)

const (
	// Common api version
	AVKubernetesV1           = "v1"
	AVAiOpsV1beta1           = "aiops.alauda.io/v1beta1"
	AVClusterV1alpha1        = "clusterregistry.k8s.io/v1alpha1"
	AVInfrastructureV1alpha1 = "infrastructure.alauda.io/v1alpha1"
	AVProjectV1              = "auth.alauda.io/v1"

	// Common resource kind for aiops
	RKAlertTemplate   = "AlertTemplate"
	RKClusterRegistry = "Cluster"
	RKClusterFeature  = "Feature"
	RKNamespace       = "Namespace"
	RKProject         = "Project"
	RKResourceQuota   = "ResourceQuota"

	// Kubernetes timestamp format
	KTimeFormat = "2006-01-02T15:04:05Z"
)

var (
	// Common label keys for aiops
	LKOwner = "alert.%s/owner"
)

func init() {
	if config.GlobalConfig.Label.BaseDomain != "" {
		LKOwner = fmt.Sprintf(LKOwner, config.GlobalConfig.Label.BaseDomain)
	} else {
		LKOwner = "alert/owner"
	}
}

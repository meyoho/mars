package model

import (
	"strconv"
	"strings"
	"time"
)

const (
	WorkloadEventReasonCount = "workload.event.reason.count"
)

type EventMeta struct {
	Indicator string
	Cluster   string
	Kind      []string
	Name      string
	Namespace string
	PodName   string
	Query     string
	Start     time.Time
	End       time.Time
}

type EventSample struct {
	Indicator string
	Cluster   string
	Kind      []string
	Name      string
	Namespace string
	PodName   string
	Query     []string
	Start     string
	End       string
}

func (m *EventMeta) ToSample() *EventSample {
	return &EventSample{
		Indicator: m.Indicator,
		Cluster:   m.Cluster,
		Kind:      m.Kind,
		Name:      m.Name,
		Namespace: m.Namespace,
		PodName:   m.PodName,
		Query:     strings.Split(m.Query, " "),
		Start:     strconv.FormatInt(m.Start.UTC().Unix()*1000000, 10),
		End:       strconv.FormatInt(m.End.UTC().Unix()*1000000, 10),
	}
}

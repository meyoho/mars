package model

import "time"

const (
	WorkloadLogKeywordCount = "workload.log.keyword.count"
)

type LogMeta struct {
	Indicator string
	Cluster   string
	Namespace string
	PodName   string
	Query     string
	Start     time.Time
	End       time.Time
}

type LogSample struct {
	Indicator string
	Cluster   string
	Namespace string
	PodName   string
	Query     string
	Start     string
	End       string
}

func (m *LogMeta) ToSample() *LogSample {
	return &LogSample{
		Indicator: m.Indicator,
		Cluster:   m.Cluster,
		Namespace: m.Namespace,
		PodName:   m.PodName,
		Query:     m.Query,
		Start:     m.Start.Format("2006-01-02T15:04:05.999999Z"),
		End:       m.End.Format("2006-01-02T15:04:05.999999Z"),
	}
}

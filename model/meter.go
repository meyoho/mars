package model

import (
	"encoding/json"
	"time"
)

type ResourceUsageRecord struct {
	ID        string    `json:"id"`
	Project   string    `json:"project"`
	Cluster   string    `json:"cluster"`
	Namespace string    `json:"namespace"`
	Pod       string    `json:"pod,omitempty"`
	Type      string    `json:"type"`
	Meter     *Meter    `json:"meter"`
	Day       string    `json:"day,omitempty"`
	StartTime time.Time `json:"startTime"`
	EndTime   time.Time `json:"endTime"`
}

type Meter struct {
	CPUUsage       float64 `json:"cpuUsage"`
	MemoryUsage    float64 `json:"memoryUsage"`
	CPURequests    float64 `json:"cpuRequests"`
	MemoryRequests float64 `json:"memoryRequests"`
}

type ResourceUsageSummaryRecord struct {
	ResourceUsageRecord `json:",inline"`
	Month               string `json:"month,omitempty"`
	Period              string `json:"period"`
}

func (r *ResourceUsageRecord) GetID() string {
	return r.ID
}

func (m *Meter) Add(right *Meter) {
	m.CPUUsage += right.CPUUsage
	m.MemoryUsage += right.MemoryUsage
	m.CPURequests += right.CPURequests
	m.MemoryRequests += right.MemoryRequests
}

func (s *ResourceUsageSummaryRecord) Add(right *ResourceUsageSummaryRecord) {
	s.Meter.Add(right.Meter)
	if s.StartTime.After(right.StartTime) {
		s.StartTime = right.StartTime
	}
	if s.EndTime.Before(right.EndTime) {
		s.EndTime = right.EndTime
	}
}

func (s *ResourceUsageSummaryRecord) GetID() string {
	return s.ID
}

func (s *ResourceUsageSummaryRecord) Clone() *ResourceUsageSummaryRecord {
	result := &ResourceUsageSummaryRecord{}
	data, err := json.Marshal(s)
	if err != nil {
		panic("clone resource usage summary record marshal error, " + err.Error())
	}
	if err := json.Unmarshal(data, result); err != nil {
		panic("clone resource usage summary record unmarshal error, " + err.Error())
	}
	return result
}

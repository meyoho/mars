package model

import (
	"strings"
)

type WorkloadItem struct {
	Kind           string
	PodNamePattern string
}

type QueryMeta struct {
	Indicator   string
	Kind        string
	Name        string
	Namespace   string
	Expr        string
	Query       string
	Function    string
	Application string
}

type QueryRequest struct {
	Time    int64             `json:"time,omitempty"`
	Queries []QueryExpression `json:"queries"`
}

type QueryRangeRequest struct {
	Start   int64             `json:"start,omitempty"`
	End     int64             `json:"end,omitempty"`
	Step    int64             `json:"step"`
	Queries []QueryExpression `json:"queries"`
}

type QueryExpression struct {
	ID         string            `json:"id,omitempty"`
	Aggregator string            `json:"aggregator"`
	Range      int64             `json:"range"`
	Labels     []LabelExpression `json:"labels"`
}

type LabelExpression struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

var (
	// Workload kinds
	Deployment  = "deployment"
	DaemonSet   = "daemonset"
	StatefulSet = "statefulset"

	// Workload set
	WorkloadSet = map[string]*WorkloadItem{
		Deployment: {
			Kind: Deployment,
			// Deployment pod example: kube-dns-5f8c75dccb-rxpbq
			PodNamePattern: "{{.Name}}-[a-z0-9]{7,10}-[a-z0-9]{5}",
		},
		DaemonSet: {
			Kind: DaemonSet,
			// DaemonSet pod example: kube-flannel-cn7hq
			PodNamePattern: "{{.Name}}-[a-z0-9]{5}",
		},
		StatefulSet: {
			Kind: StatefulSet,
			// StatefulSet pod example: prometheus-kube-prometheus-0
			PodNamePattern: "{{.Name}}-[0-9]{1,3}",
		},
	}
)

func (qe *QueryExpression) GetMeta() *QueryMeta {
	meta := QueryMeta{}
	for _, label := range qe.Labels {
		if label.Name == AlertIndicatorLabel {
			meta.Indicator = label.Value
		}
		if label.Name == AlertNameLabel {
			meta.Name = label.Value
		}
		if label.Name == AlertKindLabel {
			meta.Kind = label.Value
		}
		if label.Name == AlertNamespaceLabel {
			meta.Namespace = label.Value
		}
		if label.Name == AlertExprLabel {
			meta.Expr = label.Value
		}
		if label.Name == AlertQueryLabel {
			meta.Query = label.Value
		}
		if label.Name == AlertApplicationLabel {
			meta.Application = label.Value
		}
	}
	return &meta
}

func (qm *QueryMeta) GetPodNamePattern() string {
	if workload, ok := WorkloadSet[strings.ToLower(qm.Kind)]; ok {
		return strings.Replace(workload.PodNamePattern, "{{.Name}}", qm.Name, -1)
	} else {
		return ""
	}
}

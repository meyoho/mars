package model

import (
	"mars/config"

	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type NamespaceResource struct {
	ResourceMeta `json:",inline"`
	Spec         v1.NamespaceSpec   `json:"spec,omitempty"`
	Status       v1.NamespaceStatus `json:"status,omitempty"`
}

type NamespaceList struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`
	Items             []*NamespaceResource `json:"items,omitempty"`
}

func (n *NamespaceResource) GetProject() string {
	project, ok := n.Labels[config.GlobalConfig.Label.BaseDomain+"/project"]
	if !ok {
		return ""
	}
	return project
}

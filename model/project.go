package model

import (
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ProjectResource struct {
	ResourceMeta `json:",inline"`
	Spec         ProjectSpec            `json:"spec,omitempty"`
	Status       map[string]interface{} `json:"status,omitempty"`
}

type ProjectSpec struct {
	Clusters []*ProjectCluster `json:"clusters,omitempty"`
}

type ProjectCluster struct {
	Name  string       `json:"name"`
	Quota ClusterQuota `json:"quota"`
	Type  string       `json:"string"`
}
type ClusterQuota struct {
	LimitsCPU              string `json:"limits.cpu"`
	LimitsMemory           string `json:"limits.memory"`
	PersistentVolumeClaims string `json:"persistentvolumeclaims"`
	Pods                   string `json:"pods"`
	RequestsCPU            string `json:"requests.cpu"`
	RequestsMemory         string `json:"requests.memory"`
	RequestsStorage        string `json:"requests.storage"`
}

type ProjectList struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`
	Items             []*ProjectResource `json:"items,omitempty"`
}

package model

import (
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ResourceQuotaResource struct {
	ResourceMeta `json:",inline"`
	Spec         v1.ResourceQuotaSpec   `json:"spec,omitempty"`
	Status       v1.ResourceQuotaStatus `json:"status,omitempty"`
}

type ResourceQuotaList struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`
	Items             []*ResourceQuotaResource `json:"items,omitempty"`
}

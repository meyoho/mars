package warpgate

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"net/http"
)

// AddAuthorizationBearerTokenHeader adds a authorization bearer token header to
// the http header.
func AddAuthorizationBearerTokenHeader(header *http.Header, token string) {
	header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
}

// QueryURLForObject queries the url with bearer token and unmarshal the response body into
// the input `obj` argument.
func QueryURLForObject(url, token string, obj interface{}) (err error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return
	}
	AddAuthorizationBearerTokenHeader(&req.Header, token)
	resp, err := client.Do(req)
	if err != nil {
		return
	}

	var body []byte
	if resp.Body != nil {
		defer resp.Body.Close()
		body, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return
		}

		// Unmarshal the response body only if the response is ok.
		if resp.StatusCode == http.StatusOK {
			err = json.Unmarshal(body, obj)
		}
	}

	// Returns error if the response is not ok, the response body will be passed if not empty.
	if resp.StatusCode != 200 {
		err = errors.NewGenericServerResponse(
			resp.StatusCode, http.MethodGet,
			schema.GroupResource{Group: AlaudaFeatureGateGroup, Resource: AlaudaFeatureGateResource},
			"", string(body), 0, true)
	}
	return
}
